let timeseries = window.timeseries;
let auto_arima;
ARIMA.then(data => {
    auto_arima = data;
})

let arrayOfMethods = [
    {method: 'Авторегрессия', str: 'ar'},
    {method: 'Скользящее среднее', str: 'ma'},
    {method: 'ARIMA', str: 'auto_arima'},
];
let seasons = {
    'no': undefined,
    'hour': 3600000,
    'day': 86400000,
    'week': 604800000,
    'month': 2678400000,
    'year': 31536000000
}


let analysedData = {};
let testData;
let chartingData = {
    "isSuperAdmin": true,
    "groups": [
        {
            "id": "e3f2b6aa-9ead-4549-9305-04c2b403f865",
            "caption": "Данные",
            "tags": []
        }],
    "axis": [],
    "gdh": null
}

function processFile(files) {
    let c = files.length-1;
    for (let file of files) {
        let type = file.type.slice(12);
        var reader = new FileReader();
        reader.onload = function (e) {
            if (type === 'json') {
                console.log("JSON");
                console.log(JSON.parse(e.target.result));
                let data = JSON.parse(e.target.result);
                if (data instanceof Array){
                    c+=data.length-1;
                    data.forEach(a=>{
                        read(a);
                    })
                }
                else{
                    read(data);
                }
                endReading();
            } else {
                console.log("CSV");
                Papa.parse(e.target.result, {
                    delimiter: ';',
                    dynamicTyping: true,
                    complete: function (results) {
                        console.log("Parsing complete:", results);
                        let temp = getCSV(results);
                        c+=temp.length-1;
                        temp.forEach(a => {
                            read(a);
                        })
                        endReading();
                    }
                })
            }

        };
        reader.readAsText(file);
    }
    let count = -1;
    function read(a) {
        count++;
        readyData(a,0);
        addNewAxis(a,count);
        c--;
    }
    function endReading(){
        if (c===-1){
            console.log(chartingData);
            PullData(chartingData);
            prepareForecasting();
        }
    }
}


function getCSV(res) {
    let temp = [];
    let t;
    for (let i = 1; i < res.data[0].length; i++) {
        if (res.data[0][i] !== null) {
            t = res.data.slice(1).map((a) => {
                if (a[i] !== null) {
                    let str = a[0].replaceAll('.','-').split(' ')[0].split('-').reverse().join('-') + a[0].slice(10);
                    return [new Date(str).getTime(), a[i]]
                }
            });
            t.sort((a,b)=>{return a[0]-b[0]});
            temp.push({
                Name: res.data[0][i],
                value: t.filter((a) => {
                    if (typeof a !== 'undefined')
                        return a[1] !== null
                })
            })
        }
    }
    console.log(temp);
    return temp;
}

function readyData(values,index) {
    let d = {};
    d.id = uuidv4();
    d.caption = values.Name;
    d.isVisible = true;
    d.timeChartType = "line";
    d.axisId = index.toString();
    d.points = [];
    for (let i = 0; i < values.value.length; i++) {
        d.points.push({time: values.value[i][0], value: values.value[i][1]})
    }
    chartingData.groups[0].tags.push(d);
}

function addNewAxis(values,index){
    chartingData.axis.push({
        "id": index.toString(),
        "caption": values.Name,

    })
}

function prepareForecasting() {
    let elem = $('#window-forecasting-select')[0];
    let list = chart.series.map(a => {
        return {id: a.options.id, name: a.name}
    });
    while (elem.firstChild) {
        elem.removeChild(elem.firstChild);
    }
    for (let item of list) {
        let option = document.createElement('option');
        option.setAttribute('value', item.id);
        option.innerText = item.name;
        elem.appendChild(option);
        calculateStatistics(item.id);
    }
    $(".window-forecasting-head-selected-name")[0]
        .innerHTML = list[0].name;

    while ($('.window-forecasting-right-side-body')[0].firstChild) {
        $('.window-forecasting-right-side-body')[0].removeChild($('.window-forecasting-right-side-body')[0].firstChild);
    }
    let table = document.createElement('table');
    $('.window-forecasting-right-side-body')[0].appendChild(table);
    for (let i = 0; i < arrayOfMethods.length; i++) {
        let tableRow = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.innerText = arrayOfMethods[i].method;
        let td2 = document.createElement('td');
        let td3 = document.createElement('td');
        let button = document.createElement('input');
        button.setAttribute('type', 'button');
        button.setAttribute('value', 'Построить');
        button.addEventListener('click', (e) => {
            calcForecasting(elem.value, arrayOfMethods[i].str)
        });
        td2.appendChild(button);
        button = document.createElement('input');
        button.setAttribute('type', 'button');
        button.setAttribute('value', 'Загрузить результат');
        button.addEventListener('click', (e) => {
           downloadResults(elem.value, arrayOfMethods[i].str)
        });
        button.setAttribute('disabled', 'disabled');
        td3.appendChild(button);
        tableRow.appendChild(td1);
        tableRow.appendChild(td2);
        tableRow.appendChild(td3);
        table.appendChild(tableRow);
    }
    elem.addEventListener('change', function (e) {
        $(".window-forecasting-head-selected-name")[0]
            .innerHTML = chart.series.find(a => {
            return a.options.id === this.value
        }).name;
        if (!analysedData.hasOwnProperty(elem.value))
            calcAndWrite(elem.value);
        else writeToTable($('.window-forecasting-left-side-body')[0].childNodes[1].children, analysedData[elem.value])

    })
    elem.dispatchEvent(new Event('change'));
}

function calcAndWrite(id) {
    let elem = $('.window-forecasting-left-side-body')[0];
    let tableRows = elem.childNodes[1].children;
    calculateStatistics(id);
    writeToTable(tableRows, analysedData[id]);
}

function calculateStatistics(id) {
    console.log(id);
    let series = chart.series.find(a => {
        return a.options.id === id
    });
    let data = readyTSData(series.points);
    analysedData[id] = {};
    analysedData[id].originalData = series.points;
    analysedData[id].data = data;
    analysedData[id].min = data.min();
    analysedData[id].max = data.max();
    analysedData[id].mean = data.mean();
    analysedData[id].std = data.stdev();
    analysedData[id].yAxis = series.yAxis.userOptions.id;
}

function writeToTable(table, statistic) {
    table[0].children[1].innerText = statistic.mean.toFixed(4);
    table[1].children[1].innerText = statistic.min.toFixed(4);
    table[2].children[1].innerText = statistic.max.toFixed(4);
    table[3].children[1].innerText = statistic.std.toFixed(4);
    table[4].children[1].innerText = (statistic.max - statistic.min).toFixed(4);
}

function readyTSData(data) {
    let tsData = [];
    data.forEach(a => {
        tsData.push([new Date(a.x), a.y])
    })
    return new timeseries.main(tsData);
}

function readyDataForChart(data) {
    let b = [];
    for (let datum of data) {
        if (typeof datum === 'undefined') {
            console.log('G');
            continue;
        }
        b.push({x: Number(datum[0]), y: datum[1]})
    }
    return b
}


async function calcForecasting(id, str) {
    let data = analysedData[id];
    ts = data.data;
    let chSeries = chart.series.find(a => {
        return a.options.id === id
    });
    let series = [];
    let season = document.getElementById('window-forecasting-seasons-select').value;
    let cycles;
    let step = ts.data[1][0] - ts.data[0][0];
    let numOfPoints = 0;
    let start = ts.data[numOfPoints][0];
    numOfPoints++;
    while (ts.data[numOfPoints][0] - start < seasons[season]) {
        numOfPoints++;
    }
    cycles = Math.ceil(ts.data.length / numOfPoints / 3);
    ts.reset();
    switch (str) {
        case 'ma': {
            ts.smoother({period: Math.floor(numOfPoints / 3)});
            let temp = {
                Name: "",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }
            let arimaModel = new auto_arima({
                auto: true,
                p: 0,
                d:0,
                // q:0,
                // P:1,
                // D:1,
                // Q:0,
                s: season === 'no' ? undefined : numOfPoints
            });
            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(0);
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict( cycles * numOfPoints);
            let pr = [];
            pr.push(temp.value[trainSet.length - 1]);
            for (let prElement of predicted[0]) {
                pr.push([+pr[pr.length - 1][0] + step, prElement])
            }
            console.log(predicted);
            series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                color: chSeries.color,
                dashStyle: 'dot',
                yAxis: data.yAxis
            });
            series.push(
                {
                    id: str + '_' + id,
                    name: 'MA ' + chSeries.name,
                    type: 'line',
                    data: readyDataForChart(pr),
                    color: chSeries.color,
                    dashStyle: 'DashDot',
                    yAxis: data.yAxis
                });
            break
        }
        case 'ar': {
            ts.smoother({period: Math.floor(numOfPoints / 3)});
            let temp = {
                Name: "",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }
            let arimaModel = new auto_arima({
                auto: true,
                // p:1,
                d:0,
                q: 0,
                // P:1,
                // D:1,
                // Q:0,
                s: numOfPoints
            });
            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(0);
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict(cycles * numOfPoints);
            let pr = [];
            pr.push(temp.value[trainSet.length - 1]);
            for (let prElement of predicted[0]) {
                pr.push([+pr[pr.length - 1][0] + step, prElement])
            }
            console.log(predicted);
            series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                color: chSeries.color,
                dashStyle: 'dot',
                yAxis: data.yAxis
            });
            series.push(
                {
                    id: str + '_' + id,
                    name: 'AR ' + chSeries.name,
                    type: 'line',
                    data: readyDataForChart(pr),
                    color: chSeries.color,
                    dashStyle: 'dashdotdot',
                    yAxis: data.yAxis
                });
            break
        }
        case 'auto_arima': {
            ts.smoother({period: Math.floor(numOfPoints / 4)});
            if (season === 'no') {
                ts.smoother({period: Math.ceil(ts.original.length * 0.05)});
            }
            let pointsTRData = ts.data.slice(0, ts.original.length);
            let pointsTSData = ts.data.slice(ts.original.length);
            let temp = {
                Name: "",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }
            let arimaModel = new auto_arima({
                auto: true,
                s: season === 'no' ? undefined : numOfPoints
            });
            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(0);
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict(cycles * numOfPoints);
            let pr = [];
            pr.push(temp.value[trainSet.length - 1]);
            for (let prElement of predicted[0]) {
                pr.push([+pr[pr.length - 1][0] + step, prElement])
            }
            predicted.forEach(a => pointsTRData.push(a));
            console.log(predicted);
            series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                color: chSeries.color,
                dashStyle: 'dot',
                yAxis: data.yAxis
            });
            series.push({
                id: str + '_' + id,
                name: 'ARIMA ' + chSeries.name,
                data: readyDataForChart(pr),
                color: chSeries.color,
                dashStyle: 'LongDash',
                yAxis: data.yAxis
            })
            break
        }

    }

    for (let s of series) {
        chart.addSeries(s, true, false);
    }
    let num = arrayOfMethods.findIndex((a)=>{return a.str ===  str});
    document.getElementsByClassName('window-forecasting-right-side-body')[0]
        .children[0].children[num].children[2].children[0].removeAttribute('disabled');
}

function downloadResults(id, str) {
    let chSeries = chart.series.find(a => {
        return a.options.id === (str + '_' + id)
    });
    let points = chSeries.points;

    //CSV
    let csv_text = '';
    let delimiter = ';';
    let endSymbol = '\n';
    csv_text+='Время'+delimiter+chSeries.name+delimiter+endSymbol;
    points.forEach((a)=>{
        let x = a.x;
        x = new Date(x).toLocaleString().replaceAll(',','').replaceAll('.','-');
        csv_text+=x+delimiter+a.y+delimiter+endSymbol;
    });
    downloadFile(csv_text,chSeries.name,'application/text');

    //JSON
    let json_data = {Name:'',value:[]};
    json_data.Name = chSeries.name;
    points.forEach((a)=>{
        json_data.value.push([a.x,a.y]);
    })
    downloadFile(JSON.stringify(json_data),chSeries.name,'application/json');
}

function downloadFile(data,name,type){
    let a = document.createElement("a");
    let file = new Blob([data], {type: type});
    a.href = URL.createObjectURL(file);
    a.download = name+(type.includes('json')?'.json':'.csv');
    a.click();
}
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
