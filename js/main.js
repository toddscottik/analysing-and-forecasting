

var _skip = false;
//Автоматическое переподключение после восстановления связи
(function () {
    setInterval(function () {
            var e = document.getElementsByClassName("v-Notification-system v-position-top v-position-center");
            let desynch_base_clear = null;
            if(_currentTheme != null)
            {
                let mv_base_clear = document.querySelector('[href*="./VAADIN/themes/mv_base_clear/styles.css"]');
                if(mv_base_clear == null)
                    desynch_base_clear = document.querySelector('[href*="/VAADIN/themes/mv_base_clear/styles.css"]');
            }

            if (desynch_base_clear != null ||
                (!_skip && e.length > 0 &&  (e[0].textContent.indexOf("Session Expired") == 0 ||
                    e[0].textContent.indexOf("Communication problem") == 0 ||
                    e[0].textContent.indexOf("Internal error") == 0)))
                location.reload(true);
        },
        1000);

        setInterval(function () {
            if(window.MV_OnPing != null)
            {
                window.MV_OnPing();
            }
        }, 5000);
}
());

/*
(function () {
    if(window.MV_OnLoad == null)
    {
        let _onLoadInterval =  setInterval(function() {
            if(window.MV_OnLoad != null)
            {
                MV_OnLoad();
                clearInterval(_onLoadInterval);
            }
        }, 100);
    }
    else
    {
        MV_OnLoad();
    }


}
());

*/

let _currentTheme = null;
let _themeChanged = false;
let _newThemeLink = null;


var _skip2 = false;

function MV_ChangeTheme(theme)
{
    if(_currentTheme === theme)
        return;

    if(_skip2)
        return;


    _themeChanged = true;
    let mv_theme = null;
    //mv_theme.href =  "./VAADIN/themes/" + _curTheme + "/styles.css";

    if(_newThemeLink != null)
    {
        _newThemeLink.onload = null;
        mv_theme = _newThemeLink;
    }
    else
    {
        mv_theme = document.querySelector('[mv_theme]');
    }

    _newThemeLink = document.createElement("link");
    _newThemeLink.rel = "stylesheet";
    _newThemeLink.type = "text/css";
    _newThemeLink.href =  "./VAADIN/themes/" + theme + "/styles.css";
    _newThemeLink.onload = function ()
    {
        document.head.removeChild(mv_theme);

        let mv_base_clear = document.querySelector('[href*="./VAADIN/themes/mv_base_clear/styles.css"]');
        if(mv_base_clear != null)
            document.head.removeChild(mv_base_clear);

        _currentTheme = theme;
        _themeChanged = false;
        OnThemeChanged();
    };
    document.head.insertBefore(_newThemeLink, mv_theme.nextSibling);
    _newThemeLink.setAttribute("mv_theme", '');

    /*
    let links = document.getElementsByTagName("link");

    let styleLink;
    for(let i = 0; i < links.length; i++)
    {
        if(links[i].href.indexOf("VAADIN/themes") >= 0)
        {
            if(links[i].relList.contains("icon"))
            {
                links[i].href = "./VAADIN/themes/" + theme + "/favicon.ico";
            }
            else
            {
                styleLink = links[i];
            }
        }
    }

    */
}
function MV_CopyToClipboard(isMultiline, str)
{
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
        document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }

    MV_OnCopyToClipboard(isMultiline, str);
}


function OnThemeChanged()
{
    ContentLayoutPanel.UpdateClient(ContentLayoutPanel.mainPanel);
}


var shiftKey = false;
var altKey = false;
var ctrlKey = false;

function MV_OnUpdateKeys(e)
{
    if(typeof MV_StateKeyDown === "undefined")
        return;

    if(shiftKey !== e.shiftKey)
    {
        shiftKey = e.shiftKey;
        MV_StateKeyDown("shift", shiftKey);
        console.log("shiftKey " + shiftKey);
    }

    if(altKey !== e.altKey)
    {
        altKey = e.altKey;
        MV_StateKeyDown("alt", altKey);
        console.log("altKey " + altKey);
    }

    if(ctrlKey !== e.ctrlKey)
    {
        ctrlKey = e.ctrlKey;
        MV_StateKeyDown("ctrl", ctrlKey);
        console.log("ctrlKey " + ctrlKey);
    }
};
/*
var _lost = function()
{
    if(typeof MV_StateKeyDown === "undefined")
        return;

    _updateKeys({
        shiftKey: false,
        altKey: false,
        ctrlKey: false
    });
};
*/
window.onkeydown = MV_OnUpdateKeys;
window.onkeyup = MV_OnUpdateKeys;
//window.onclick = _updateKeys;
//window.onfocus = _lost;
//window.onblur = _lost;

var Mouse =
{
    clientX: 0,
    clientY: 0,
    pageX: 0,
    pageY: 0,
};


window.addEventListener('mousemove', function(event) {
    _mouseMove(event, false);
}, true);

window.addEventListener('drag', function(event) {
    event.dataTransfer.effectAllowed = 'all';
    MV_OnUpdateKeys(event);
    _mouseMove(event, true);
    ContentLayoutPanel.FindDropPanel();
}, true);


let _hideDraggingCurtain = false;

window.addEventListener('mousedown', function(event) {
    document.getElementsByTagName("body")[0].classList.add("i-hide-draggingCurtain");//Заплатка против бага, когда завеса появляется, без движения курсора
    _hideDraggingCurtain = true;
}, true);


function _mouseMove(event, isDrag)
{
    if(!isDrag && event.movementX == 0 && event.movementY == 0)
        return;

    var eventDoc, doc, body;

    event = event || window.event;

    if(_hideDraggingCurtain)
    {
        document.getElementsByTagName("body")[0].classList.remove("i-hide-draggingCurtain");
        _hideDraggingCurtain = false;
    }

    Mouse.pageX = 0;
    Mouse.pageY = 0;

    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        Mouse.pageX = event.clientX +
            (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
            (doc && doc.clientLeft || body && body.clientLeft || 0);
        Mouse.pageY = event.clientY +
            (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
            (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }
    else
    {
        Mouse.pageX = event.pageX;
        Mouse.pageY = event.pageY;
    }

    Mouse.clientX = event.clientX;
    Mouse.clientY = event.clientY;

    //MV_MouseState(Mouse.pageX, Mouse.pageY, Mouse.clientX, Mouse.clientY);
}

//function MV_MouseState() {}




function MV_SetScrollPosition(id, scrollLeft, scrollTop)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    scrollH.scrollLeft = scrollLeft;
    scrollV.scrollTop = scrollTop;
}
function MV_RequestScrollPosition(id)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    if(scrollH.onscroll != null || scrollV.onscroll != null)
        return;

    var _fireUpdateScrollPos = function()
    {
        _updateScrollPos(id, scrollH, scrollV);
    };

    scrollH.onscroll = _fireUpdateScrollPos;
    scrollV.onscroll = _fireUpdateScrollPos;

    _updateScrollPos(id, scrollH, scrollV);
}

function MV_StopRequestScrollPosition(id)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    scrollH.onscroll = null;
    scrollV.onscroll = null;
}


function _updateScrollPos(id, scrollH, scrollV)
{
    MV_RequestScrollPositionListener(
        id,
        {
            scrollLeft:     scrollH.scrollLeft,
            scrollTop:      scrollV.scrollTop
        });
}




function MV_RequestBoundingClientRect(id, isRelative)
{
    var obj = document.getElementById(id);
    var rect = obj.getBoundingClientRect();

    var scrollX = isRelative ? window.scrollX : 0;
    var scrollY = isRelative ? window.scrollY : 0;

    MV_RequestBoundingListener(
        id,
        {
            m_Left:     rect.left   + scrollX,
            m_Right:    rect.right  + scrollX,
            m_Bottom:   rect.bottom + scrollY,
            m_Top:      rect.top    + scrollY,

            m_Height:   rect.height,
            m_Width:    rect.width,

            m_X:        rect.x + scrollX,
            m_Y:        rect.y + scrollY
        });
}



var ContentLayoutPanel = {
    contentLayoutPanel: null,
    mainPanel: null,
    splitPanels: null,
    focusPanel: null,
    targetPosition: null,
    isDragged: false,
    isInit: -1,
    draggedSplitter: null,
    mainStyleSheet: null,
    UpdateClient: function (mainPanel)
    {

        if(this.isInit === -1)
            this.isInit = 0;

        mainPanel.parent = null;
        mainPanel.index = -1;

        this.mainPanel = mainPanel;
        this.splitPanels = {};
        this.focusPanel = null;

        if(this.isInit === 0)
        {
            setTimeout('ContentLayoutPanel._init()',0);
        }
        else
        {
            setTimeout('ContentLayoutPanel._doParceUpdate()',0);
        }
    },
    _doParceUpdate:function()
    {
        this._parsePanel(this.mainPanel);
        this.Update(this.mainPanel);
    },
    _init:function()
    {
        this.mainStyleSheet =  document.getElementsByTagName("style")[0].sheet;

        this.contentLayoutPanel = function()
        {
            return document.getElementById("ContentLayoutPanel");
        };


        window.addEventListener("resize", _debounce(function(e){
            ContentLayoutPanel.Update(ContentLayoutPanel.mainPanel);
            //_update();
        }));

        window.addEventListener("mouseup", function(event) {
            if(ContentLayoutPanel.draggedSplitter != null)
            {
                if(ContentLayoutPanel.draggedSplitter.parentPanel.isDirty)
                    ContentLayoutPanel.Update(ContentLayoutPanel.draggedSplitter.parentPanel);

                ContentLayoutPanel.contentLayoutPanel().setAttribute("state", "");
                ContentLayoutPanel.draggedSplitter = null;
            }

        },true);

        window.addEventListener("mousedown", function(event)
        {
            if(ContentLayoutPanel.draggedSplitter != null)
                return;

            var _splitter = event.target;
            if(_splitter.classList.contains("v-splitpanel-hsplitter") || _splitter.classList.contains("v-splitpanel-vsplitter"))
            {
                var _splitPanel = _splitter.parentElement.parentElement;

                ContentLayoutPanel.draggedSplitter = ContentLayoutPanel.splitPanels[_splitPanel.id];
            }
            else
            {
                ContentLayoutPanel.draggedSplitter = null;
            }

            if(ContentLayoutPanel.draggedSplitter != null)
                ContentLayoutPanel.contentLayoutPanel().setAttribute("state", "active");

            //console.log("mousedown")
        }, true);

        window.addEventListener("mousemove", function(event)
        {
            if(ContentLayoutPanel.draggedSplitter == null)
                return;

            if(ContentLayoutPanel._moveSplitter(ContentLayoutPanel.draggedSplitter, null))
            {
                ContentLayoutPanel.draggedSplitter.parentPanel.isDirty = true;
                //ContentLayoutPanel.Update(ContentLayoutPanel.draggedSplitter.parentPanel);
            }

            //ContentLayoutPanel.Update([_s.firstPanel, _s.secondPanel]);

        }, true);

        this.isInit = 1;

        ContentLayoutPanel._doParceUpdate();
    },
    _updateSplitPanels: function(splitPanel, startPanelIndex, endPanelIndex, curIndex, direction)
    {
        if(startPanelIndex > endPanelIndex)
            return;

        var _s = splitPanel.splits[startPanelIndex];
        this._updateSplitPanels(_s.firstPanel, 0, _s.firstPanel.splits.length - 1, -1, direction);

        for(var i = startPanelIndex; i <= endPanelIndex; i++)
        {
            _s = splitPanel.splits[i];


            if(splitPanel.direction === direction && i != curIndex)
            {
                var _splitPanelWrapper = _s.GetSplitPanelWrapper();
                var _splitter = _s.GetSplitterElement();

                if(splitPanel.direction === "Horizontal")
                {
                    _splitter.style.left =  Math.round(_splitPanelWrapper.offsetWidth * _s.position) + "px";
                }
                else
                {
                    _splitter.style.top =  Math.round(_splitPanelWrapper.offsetHeight * _s.position) + "px";
                }

            }
            this._updateSplitPanels(_s.secondPanel, 0, _s.secondPanel.splits.length - 1, -1, direction);
        }
    },
    _moveSplitter: function(_split, position)
    {

        var _splitter = _split.GetSplitterElement();
        var _splitPanelWrapper = _split.GetSplitPanelWrapper();

        var _firstElement = _split.firstPanel.GetElement().parentElement;
        var _secondElement = _split.secondPanel.GetElement().parentElement;

        //var _parent = _split.parentPanel.GetElement();

        if(position != null)
        {
            _split.position = Math.round(position * 1000) / 1000;
        }

        if(_split.parentPanel.direction === "Horizontal")
        {
            if(position == null)
            {
                var _width = Math.max(1, _splitPanelWrapper.offsetWidth);

                var newPosition = Math.round((_splitter.offsetLeft + _splitter.offsetWidth * 0.5) / _width * 1000) / 1000;
                if(newPosition === _split.position)
                    return false;

                _split.position = newPosition
            }

            var _pos = "(100% - (" + _splitPanelWrapper.style.left + " + " + _splitPanelWrapper.style.right + ")) * " + _split.position + " + " + _splitPanelWrapper.style.left;

            if(_split.index === 0)
                _firstElement.style.left = "0px";
            if(_split.index === _split.parentPanel.splits.length - 1)
                _secondElement.style.right = "0px";

            _firstElement.style.right = "calc(100% - (" + _pos + "))";
            _secondElement.style.left = "calc(" + _pos + ")";
        }
        else
        {
            if(position == null)
            {
                var _height = Math.max(1, _splitPanelWrapper.offsetHeight);
                var newPosition = Math.round((_splitter.offsetTop + _splitter.offsetHeight * 0.5) / _height * 1000) / 1000;
                if(newPosition === _split.position)
                    return false;

                _split.position = newPosition
            }


            var _pos = "(100% - (" + _splitPanelWrapper.style.top + " + " + _splitPanelWrapper.style.bottom + ")) * " + _split.position + " + " + _splitPanelWrapper.style.top ;

            if(_split.index === 0)
                _firstElement.style.top = "0px";
            if(_split.index === _split.parentPanel.splits.length - 1)
                _secondElement.style.bottom = "0px";

            _firstElement.style.bottom = "calc(100% - (" + _pos + "))";
            _secondElement.style.top = "calc(" + _pos + ")";
        }


        if(position == null)
        {
            var startPanelIndex = _split.index;
            var endPanelIndex = _split.index;

            for(var i = 0; i < _split.parentPanel.splits.length; i++)
            {
                var _s = _split.parentPanel.splits[i];

                if(i < _split.index)
                {
                    if(_s.position > _split.position)
                    {
                        this._moveSplitter(_s, _split.position);

                        if(i < startPanelIndex)
                            startPanelIndex = i;
                    }
                }
                else if(i > _split.index)
                {
                    if(_s.position < _split.position)
                    {
                        this._moveSplitter(_s, _split.position);

                        if(i > endPanelIndex)
                            endPanelIndex = i;
                    }
                }
            }

            this._updateSplitPanels(_split.parentPanel, startPanelIndex, endPanelIndex, _split.index, _split.parentPanel.direction);
        }

        return true;
    },
    _parsePanel: function(curPanel)
    {
        /*
        curPanel.GetElement = function(updateCache)
        {
            return this._cacheElement == null ? document.getElementById(this.id) : this._cacheElement;
        };

        curPanel.GetFormElement = function()
        {
            return this.form == null ? null : document.getElementById(this.form);
        };*/

        curPanel.GetElement = function()
        {
            return document.getElementById(this.id);
        };

        curPanel.GetFormElement = function()
        {
            return document.getElementById(this.form);
        };


        var panelStyle = this._getStyleRule(".i-DropPanel", curPanel.id);

        panelStyle.setProperty("min-width", curPanel.minWidth + "px", 'important');
        panelStyle.setProperty("min-height", curPanel.minHeight + "px", 'important');


        //sheet.$$("cells").getItem(3)[2]

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            var _s = curPanel.splits[i];

            _s.firstPanel = curPanel.childs[i];
            _s.secondPanel = curPanel.childs[i + 1];
            _s.parentPanel = curPanel;
            _s.index = i;

            _s.GetSplitterElement = function()
            {
                return document.getElementById(this.id).childNodes[0].childNodes[1];
            };

            _s.GetSplitPanelWrapper = function()
            {
                return document.getElementById(this.id).parentElement;
            };

            this.splitPanels[_s.id] = _s;
        }

        for(var i = 0; i < curPanel.childs.length; i++)
        {
            curPanel.childs[i].parent = curPanel;
            curPanel.childs[i].index = i;

            this._parsePanel(curPanel.childs[i]);
        }

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            var _s = curPanel.splits[i];

            //_s.firstPanel.GetElement(true);
            //_s.secondPanel.GetElement(true);

            ContentLayoutPanel._moveSplitter(_s, _s.position);
        }
    },
    Update: function(panel)
    {
        if(this.isInit !== 1)
            return;

        //console.log("Update")

        var panelTransforms = {};

        ContentLayoutPanel._updatePanel(panel, panelTransforms);
        MV_ContentLayoutPanel_UpdateServer(panelTransforms);


        panel.isDirty = false;
    },
    _updatePanel: function(curPanel, panelTransforms)
    {
        if(curPanel == null)
            return;

        var rect = curPanel.GetElement();
        var form = curPanel.GetFormElement();

        if(rect == null)
        {
            var gg = "";
            gg += "we";

            return;
        }

        var bRect = rect.getBoundingClientRect();


        var _left = Math.round(bRect.left);
        var _top = Math.round(bRect.top);
        var _width = Math.round(bRect.width);
        var _height = Math.round(bRect.height);

        var _splitsPositions = [];

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            _splitsPositions.push(curPanel.splits[i].position);
        }

        panelTransforms[curPanel.id] = {
            left: _left,
            top: _top,
            width: _width,
            height: _height,
            splitsPositions: _splitsPositions
        };

        if(form != null/* && form.classList.contains("i-is-embedded")*/)
        {
            form.style.left = _left + "px";
            form.style.top = _top + "px";
            form.style.width = _width + "px";
            form.style.height = _height + "px";
        }

        for(var i = 0; i < curPanel.childs.length; i++)
        {
            this._updatePanel(curPanel.childs[i], panelTransforms);
        }
    },
    _getStyleRule: function(selector, id)
    {
        var styleRule = null;
        for(var i = 0; i < this.mainStyleSheet.rules.length; i++)
        {
            if(this.mainStyleSheet.rules[i].selectorText.indexOf(selector + "#" + id) >= 0)
            {
                styleRule = this.mainStyleSheet.rules[i];
                break;
            }
        }
        if(styleRule == null)
            styleRule = this.mainStyleSheet.rules[this.mainStyleSheet.insertRule(selector + "#" + id + "{}")];

        return styleRule.style;
    },
    Start: function ()
    {
        if(this.isInit !== 1)
            return;

        this.isDragged = true;
    },
    Release: function ()
    {
        if(this.isInit !== 1 || !this.isDragged)
            return;

        this.isDragged = false;

        if(this.focusPanel == null)
        {
            MV_OnReleaseContentLayoutPanel(null, null);
            return;
        }

        MV_OnReleaseContentLayoutPanel(this.focusPanel.id, this.targetPosition);

        this.focusPanel.GetElement().setAttribute("active", null);

        this.focusPanel = null;
    },
    FindDropPanel: function ()
    {
        if(this.isInit !== 1 || !this.isDragged)
            return;

        var focusPanel = this._findFocusPanel(this.mainPanel);
        var targetPosition = focusPanel == null ? null : this._calcTargetPosition(focusPanel, focusPanel.GetElement().getBoundingClientRect());

        while(!(focusPanel == null ||
                targetPosition == null ||
                targetPosition === "center" || targetPosition === "left" || targetPosition === "right" || targetPosition === "top" || targetPosition === "bottom"))
        {
            if(focusPanel === this.mainPanel)
            {
                if(targetPosition === "top_left" || targetPosition === "top_right")
                    targetPosition = "top";
                else if(targetPosition === "left_top" || targetPosition === "left_bottom")
                    targetPosition = "left";
                else if(targetPosition === "right_top" || targetPosition === "right_bottom")
                    targetPosition = "right";
                else if(targetPosition === "bottom_left" || targetPosition === "bottom_right")
                    targetPosition = "bottom";

                break;
            }

            if(focusPanel.direction === "Horizontal")
            {
                if(focusPanel.index === 0 &&
                    (targetPosition === "left_top" || targetPosition === "top_left" || targetPosition === "right_top" || targetPosition === "top_right"))
                {
                }
                else if(focusPanel.index === (focusPanel.parent.childs.length - 1) &&
                    (targetPosition === "left_bottom" || targetPosition === "bottom_left" || targetPosition === "right_bottom" || targetPosition === "bottom_right"))
                {
                }
                else
                {
                    if(targetPosition === "left_top" || targetPosition === "top_left" ||
                        targetPosition === "left_bottom" || targetPosition === "bottom_left")
                    {
                        targetPosition = "left";
                    }
                    else if(targetPosition === "right_bottom" || targetPosition === "bottom_right" ||
                        targetPosition === "right_top" || targetPosition === "top_right")
                    {
                        targetPosition = "right";
                    }

                    //break;
                }
            }
            else if(focusPanel.direction === "Vertical")
            {
                if(focusPanel.index === 0 &&
                    (targetPosition === "top_left" || targetPosition === "left_top" || targetPosition === "bottom_left" || targetPosition === "left_bottom"))
                {
                }
                else if(focusPanel.index === (focusPanel.parent.childs.length - 1) &&
                    (targetPosition === "top_right" || targetPosition === "right_top" || targetPosition === "bottom_right" || targetPosition === "right_bottom"))
                {
                }
                else
                {
                    if(targetPosition === "top_right" || targetPosition === "right_top" ||
                        targetPosition === "top_left" || targetPosition === "left_top")
                    {
                        targetPosition = "top";
                    }
                    else if(targetPosition === "bottom_right" || targetPosition === "right_bottom" ||
                        targetPosition === "bottom_left" || targetPosition === "left_bottom")
                    {
                        targetPosition = "bottom";
                    }

                    //break;
                }
            }

            focusPanel = focusPanel.parent;
        }

        if(this.focusPanel != null && this.focusPanel !== focusPanel)
            this.focusPanel.GetElement().setAttribute("active", null);

        this.focusPanel = focusPanel;

        if(focusPanel != null)
        {
            this.targetPosition = targetPosition;
            this.focusPanel.GetElement().setAttribute("active", this.targetPosition);
        }
        else
        {
            this.targetPosition = null;
        }

    },
    _findFocusPanel: function (curPanel)
    {
        var bRect = curPanel.GetElement().getBoundingClientRect();

        if(Mouse.clientX >= bRect.x && Mouse.clientX <= bRect.x + bRect.width &&
            Mouse.clientY >= bRect.y && Mouse.clientY <= bRect.y + bRect.height)
        {
            for(var i = 0; i < curPanel.childs.length; i++)
            {
                var panel = this._findFocusPanel(curPanel.childs[i]);
                if(panel != null)
                    return panel;
            }
            return curPanel;
        }

        return null;
    },
    _calcTargetPosition: function (focusPanel, bRect) {
        var maxBorderWidth  = Math.min(200, bRect.width * 0.25);
        var maxBorderHeight = Math.min(200, bRect.height * 0.25);


        if(focusPanel === this.mainPanel && focusPanel.form == null)
            return "center";

        if( Math.abs(Mouse.clientX - (bRect.x + bRect.width / 2)) <=  Math.max(bRect.width * 0.25, (bRect.width * 0.5 - maxBorderWidth)) &&
            Math.abs(Mouse.clientY - (bRect.y + bRect.height / 2)) <=  Math.max(bRect.height * 0.25, (bRect.height * 0.5 - maxBorderHeight)))
        {
            return "center";
        }

        var leftRange = Mouse.clientX - bRect.x;
        var rightRange = bRect.x + bRect.width - Mouse.clientX;

        var topRange = Mouse.clientY - bRect.y;
        var bottomRange = bRect.y + bRect.height - Mouse.clientY;

        if(leftRange <= maxBorderWidth)
        {
            if(topRange <= maxBorderHeight)
            {
                if(leftRange < topRange)
                {
                    return "left_top";
                }
                else
                {
                    return "top_left";
                }
            }
            else if(bottomRange <= maxBorderHeight)
            {
                if(leftRange < bottomRange)
                {
                    return "left_bottom";
                }
                else
                {
                    return "bottom_left";
                }
            }
            else
            {
                return "left";
            }
        }
        else if(rightRange <= maxBorderWidth)
        {
            if(topRange <= maxBorderHeight)
            {
                if(rightRange < topRange)
                {
                    return "right_top";
                }
                else
                {
                    return "top_right";
                }
            }
            else if(bottomRange <= maxBorderHeight)
            {
                if(rightRange < bottomRange)
                {
                    return "right_bottom";
                }
                else
                {
                    return "bottom_right";
                }
            }
            else
            {
                return "right";
            }
        }
        else if(topRange <= maxBorderHeight)
        {
            return "top";
        }
        else if(bottomRange <= maxBorderHeight)
        {
            return "bottom";
        }

        return null;
    }
};


function _debounce(func){
    var timer;
    return function(event){
        if(timer) clearTimeout(timer);
        timer = setTimeout(func,500,event);
    };
};
