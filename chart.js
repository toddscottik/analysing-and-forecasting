var chart;

let button;
let showPlotLinesButton;
let buttonFillColor = getComputedStyle(document.querySelector(':root')).getPropertyValue('--main-object-fill-color');
let buttonSelectColor = getComputedStyle(document.querySelector(':root')).getPropertyValue('--main-object-fill-color-inversed');
let buttonHoverColor = getComputedStyle(document.querySelector(':root')).getPropertyValue('--main-object-shadow-100-color');
let buttonStrokeColor = getComputedStyle(document.querySelector(':root')).getPropertyValue('--main-object-stroke-color');

let forecastingButton;

buttonStyles = [
    {
        'fill': 'rgba(0, 0, 0, 0)',
        'stroke': "black",
        'stroke-width': 2
    },
    {
        'fill': "var(--main-object-shadow-100-color)",
        'stroke': "black",
        'stroke-width': 2
    },
    {
        'fill': "black",
        'stroke': "black",
        'stroke-width': 2
    },
];

window.onload = function () {
    console.log("Chart onLoad");
    _updateStatic();
    updateSVG();
};


function _updateStatic() {
    chart = Highcharts.chart('container', {
        time: {
            useUTC: false
        },
        chart: {
            animation: false,
            type: 'line',
            backgroundColor: null,
            style: {
                fontFamily: 'Consolas, sans-serif',
                color: "black"
            },
            zoomType: 'xy',
            //styledMode: true,
            panning: true,
            panKey: 'shift',
            // resetZoomButton: {
            //     theme: {
            //         display: 'none'
            //     }
            // }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            reversed: false,
            lineColor:  "black",
            tickColor:  "black",
            gridLineColor:  "black",
            labels: {
                style: {
                    color: 'black',
                    fontSize: "11px",
                    textAlign: "center"
                },
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e. %b',
                    week: '%e. %b',
                    month: '%b \'%y',
                    year: '%Y'
                }
            },
            maxPadding: 0.05,
            showLastLabel: true,
            title: {
                style: {
                    color:  "black"
                }
            },
            crosshair: true,
            scrollbar: {
                enable: true
            },
            events: {
                afterSetExtremes:

                    _debounce(function (e) {
                        updatePlotLines();
                    })

            }
        },
        yAxis: [],
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                threshold: null,
                borderWidth: 0,
                turboThreshold: 50000,
                fillOpacity: 0.2,
                marker: {
                    enabled: false,
                    lineWidth: null,
                    radius: 2
                },
                states: {
                    hover: {
                        animation: {
                            duration: 0
                        },
                        halo: {
                            size: 0
                        },
                        lineWidthPlus: 1

                    },
                    normal: {
                        animation: false
                    }
                },
                lineWidth: 2
            },
            column: {
                pointPadding: 0.1,
                groupPadding: 0.1
            },
            area: {
                lineWidth: 1.3
            },
            areastep: {
                lineWidth: 1.3
            },
            areaspline: {
                lineWidth: 1.3
            }
        },
        series: [],
        tooltip: {
            shared: true,
            dateTimeLabelFormats: {
                millisecond: '%A, %Y %b %e, %H:%M:%S.%L',
                second: '%A, %Y %b %e, %H:%M:%S',
                minute: '%A, %Y %b %e, %H:%M',
                hour: '%A, %Y %b %e, %H:%M',
                day: '%A, %Y %b %e, %Y',
                week: '%A, %Y %b %e, %Y',
                month: '%B %Y',
                year: '%Y'
            },
            outside: true,
            useHTML: true
        },
        exporting: {
            chartOptions: {
                legend: {
                    enabled: true
                }
            },
        },
        credits: {
            enabled: false
        }
    });

    var highchartsOptions = Highcharts.setOptions({
        lang: {
            month: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
            weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
        }
    });
    button = chart.renderer.button("", chart.plotSizeX - 20, chart.plotTop + 5, setMode, buttonStyles[0], buttonStyles[1], buttonStyles[2], undefined, 'diamond')
        .attr({
            zIndex: 8
        });
    showPlotLinesButton = chart.renderer.button("", chart.plotSizeX - 45, chart.plotTop + 5, showPlotLines, buttonStyles[2], buttonStyles[1], buttonStyles[0], undefined, 'rect')
        .attr({
            zIndex: 8
        });
    forecastingButton = chart.renderer.button("", chart.plotSizeX + chart.plotLeft - 20, chart.plotTop + 80, showForecastingWindow,
        !isShowForecasting ? buttonStyles[0] : buttonStyles[2], buttonStyles[1], buttonStyles[2], undefined, 'triangle')
        .attr({
            zIndex: 8,
        }).add();
}

Highcharts.wrap(Highcharts.Pointer.prototype, 'onContainerMouseDown', function (proceed, e) {
    var popupClass = e.target && e.target.className;
    // elements is not in popup
    if (!(Highcharts.isString(popupClass) &&
        popupClass.indexOf('highcharts-number-select') >= 0)) {
        proceed.apply(this, Array.prototype.slice.call(arguments, 1));
    }

});

var _updateAxis22;
var _currentSeries;
let tickPosX;
let isAllPointsInOneX = false;
let grouping = false;

function PullData(data) {
    _testData = data;

    document.getElementById("blocker").style.display = data.groups !== undefined && data.groups.length > 0 ? "none" : "flex";

    if (data.groups !== undefined) {
        let doRedraw = false;

        for (var g = 0; g < data.groups.length; g++) {
            let localePrevX;
            var group = data.groups[g];

            if (group.tags.length === 0)
                continue;


            var _updateSeries = [];
            var _updateAxis = [];

            var containsAxisGuid = [];

            for (let a = 0; a < data.axis.length; a++) {
                let axisData = data.axis[a];
                let axisId = axisData.id;
                let visibleAxis = false;
                for (let t = 0; t < group.tags.length; t++) {
                    if (group.tags[t].isVisible && group.tags[t].axisId === axisId) {
                        visibleAxis = true;
                        break;
                    }
                }
                if (!visibleAxis)
                    continue;
                _updateAxis.push(_createAxis(axisData, visibleAxis, containsAxisGuid.length));
                containsAxisGuid.push(axisId);
            }
            var _ids = Object.keys(_axisIds);

            for (let i = 0; i < _ids.length; i++) {
                let _id = _ids[i];
                if (containsAxisGuid.indexOf(_id) === -1)
                    delete _axisIds[_id];
            }

            var containsTags = [];
            for (let t = 0; t < group.tags.length; t++) {
                let tagData = group.tags[t];
                _updateSeries.push(_createSeries(tagData));
            }
            let k;
            k = false;
            for (let a of _updateAxis) {
                if (a.gridLineWidth) {
                    k = true;
                }
            }
            if (!k) {
                _updateAxis[0].gridLineWidth = 1;
            }


            chart.update({
                yAxis: _updateAxis,
                series: _updateSeries
            }, true, true, false);
            _updateAxis22 = _updateAxis;

        }

    }

    updatePlotLines();

    mode = !mode;
    setMode();
    isShowPlotLines = !isShowPlotLines;
    showPlotLines();
    createHintLabels();
    updateSVG();


}


function _createSeries(tagData) {
    let _d = [];

    let _type = tagData.timeChartType;
    let _step = undefined;
    let _dashStyle = 'solid';

    for (let p = 0; p < tagData.points.length; p++) {
        let point = tagData.points[p];

        let showMarker = true;
        if (p > 0)
            showMarker &= !tagData.points[p - 1].value;
        if (p < tagData.points.length - 1)
            showMarker &= !tagData.points[p + 1].value;

        _d.push({x: point.time, y: point.value, marker: {enabled: showMarker}});

    }
    let _opacity = 1;

    return {
        id: tagData.id,
        visible: tagData.isVisible,
        data: _d,
        type: _type,
        step: _step,
        dashStyle: _dashStyle,
        name: tagData.caption,
        color: tagData.color,
        opacity: _opacity,
        zIndex:4,
        yAxis: tagData.axisId === undefined ? 0 : _axisIds[tagData.axisId]
    };
}

var _axisIds = {};

function _createAxis(axisData, visibleAxis, index) {
    _axisIds[axisData.id] = index;
    return {
        id: index,
        visible: visibleAxis,
        type: "linear",
        lineColor: '#313b4ecc',
        tickColor: '#313b4ecc',
        gridLineColor: '#313b4ecc',
        title: {
            enabled: axisData.caption !== undefined,
            align: 'middle',
            margin: 10,
            style: {
                color:  "black"
            },
            text: axisData.caption !== undefined ? axisData.caption : ""
        },
        labels: {
            format: '{value}',
            style: {
                color:  "black",
                fontSize: "11px"
            },
        },
        lineWidth: 2,
        crosshair: false
    };
}


var _plotLines = [];
var _plotLinesUpLabels = [];
var _plotLinesDownLabels = [];

function createPlotLines(e) {
    let id = Date.now();
    _plotLines[id] = chart.xAxis[0].addPlotLine({
        color: Highcharts.getOptions().tooltip.style.backgroundColor,
        id: id,
        label: "LINE",
        value: chart.pointer.getCoordinates(e).xAxis[0].value,
        width: 2,
        zIndex: 6
    });
    _plotLines[id].svgElem.css({'cursor': 'ew-resize'}).attr({id: id}).on('mousedown', start);

    _plotLinesDownLabels[id] = chart.renderer.label(
        '', e.chartX, chart.plotTop + chart.plotHeight
    ).attr({
        fill: Highcharts.getOptions().tooltip.backgroundColor,
        stroke: Highcharts.getOptions().tooltip.style.color,
        zIndex: 8,
        'stroke-width': 1,
    }).css({color: Highcharts.getOptions().tooltip.style.color}).add();
    _plotLinesUpLabels[id] = chart.renderer.label(
        '', e.chartX, chart.plotTop
    ).attr({
        fill: Highcharts.getOptions().tooltip.backgroundColor,
        stroke: Highcharts.getOptions().tooltip.style.color,
        zIndex: 8,
        'stroke-width': 1,
    }).css({color: Highcharts.getOptions().tooltip.style.color}).add();

    createPlotLinesLabels(chart.pointer.getCoordinates(e).xAxis[0].value, id);
}

let sortPointID = [];
let pointData = {};
let viewPointLineDelta = true;

function createPlotLinesLabels(x, id) {
    let resL = '';
    let resU = '';
    let param;

    //line approximation
    let series = Object.create(null);
    for (let i of chart.series) {
        if (!i.visible) continue;
        param = Object.create(null);
        if (series[i.userOptions.id] !== undefined)
            param = series[i.userOptions.id];

        let prev_min = null;
        let prev_max = null;
        let prev_avg = null;
        let prev_x = null;
        for (let j of i.points) {
            if (j.y === null) continue;
            if (j.x < x) {
                prev_x = j.x;
                if (i.userOptions.type === 'line' || i.userOptions.type === "step" || i.userOptions.type === "spline"
                    || i.userOptions.type === 'arearange' || i.userOptions.type === "areaspline" ||
                    i.userOptions.type === "arearange" ||
                    i.userOptions.type === "areasplinerange" || i.userOptions.type === "areastep" || i.userOptions.type === "area"
                    || i.userOptions.type === "column")
                    prev_avg = j.y;
            } else if (j.x > x) {
                if (prev_x === null)
                    break;
                let k = null;
                let b = null;
                if (i.userOptions.type === 'line' || i.userOptions.type === "step" || i.userOptions.type === "spline"
                    || i.userOptions.type === 'arearange' || i.userOptions.type === "areaspline" ||
                    i.userOptions.type === "arearange" ||
                    i.userOptions.type === "areasplinerange" || i.userOptions.type === "areastep" || i.userOptions.type === "area"
                    || i.userOptions.type === "column") {
                    if (prev_avg === null)
                        break;
                    k = (j.y - prev_avg) / (j.x - prev_x);
                    b = prev_avg - k * prev_x;
                    param.avg = k * x + b;
                }
                series[i.userOptions.id] = param;
                break;
            } else if (j.x === x) {
                if (i.userOptions.type === 'line' || i.userOptions.type === "step" || i.userOptions.type === "spline"
                    || i.userOptions.type === 'arearange' || i.userOptions.type === "areaspline" ||
                    i.userOptions.type === "arearange" ||
                    i.userOptions.type === "areasplinerange" || i.userOptions.type === "areastep" || i.userOptions.type === "area"
                    || i.userOptions.type === "column")
                    param.avg = j.y;
                series[i.userOptions.id] = param;
                break;
            }

        }
    }

    pointData[id] = series;
    let sortPoint = [];
    for (let j of Object.keys(series)) {
        let maxVal = Number.MIN_SAFE_INTEGER;
        let curID = null;
        for (let i of Object.keys(series)) {
            param = series[i];
            if (param !== undefined) {
                if (param.avg !== undefined) {
                    if (param.avg >= maxVal && sortPoint.indexOf(i) === -1) {
                        maxVal = param.avg;
                        curID = i;
                    }
                } else if (param.max !== undefined) {
                    if (param.max >= maxVal && sortPoint.indexOf(i) === -1) {
                        maxVal = param.max;
                        curID = i;
                    }
                }
            }
        }
        if (curID !== null)
            sortPoint.push(curID);
    }
    sortPointID[id] = sortPoint;

    let points = Object.create(null);
    for (let i of chart.xAxis[0].plotLinesAndBands) {
        let point_id = i.id;
        if (_plotLines[point_id] !== undefined) {
            let point_x = i.options.value;
            if (point_id === id)
                point_x = x;
            if (points[point_x] !== undefined) {
            } else
                points[point_x] = point_id;
        }
    }
    if (isRemoveCurPlotLine) {
        pointData[id] = 'del';
    }
    let prevVals = Object.create(null);
    let keys = Object.keys(points).sort();
    for (let j of keys) {
        resU = '';
        let pid = points[j];

        if (pointData[pid] === 'del') {
            _plotLinesUpLabels[pid].attr({text: '<span style="font-size: 10px">УДАЛИТЬ</span>'});
            _plotLinesDownLabels[pid].attr({text: '<span style="font-size: 10px">УДАЛИТЬ</span>'});
        } else {
            x = new Date(parseInt(j));
            resL = '<span style="font-size: 10px">' + chart.time.dateFormat('%H:%M:%S', x) + '</span><br>' +
                '<span style="font-size: 10px">' + chart.time.dateFormat('%Y-%m-%d', x) + '</span>';
            let ser = pointData[pid];
            let sortID = sortPointID[pid];
            for (let i of sortID) {
                param = ser[i];
                if (param !== undefined) {
                    if (resU !== '')
                        resU = resU + '<br>';
                    resU = resU + '<span style="color:' + chart.get(i).color + '">\u25CF</span><span style="font-size: 10px"> ';
                    if (param.avg !== undefined && param.max !== undefined && param.min !== undefined) {
                        if (prevVals[i] === undefined)
                            resU = resU + round(param.avg) + ' [' + round(param.min) + ' - ' + round(param.max) + ']';
                        else {
                            resU = resU + round(param.avg) + ' [' + round(param.min) + ' - ' + round(param.max) + ']';
                            if (viewPointLineDelta)
                                resU = resU + ' (\u0394 ' + round(param.avg - prevVals[i]) + ')';
                        }
                        prevVals[i] = param.avg;
                    } else if (param.avg !== undefined) {
                        if (prevVals[i] === undefined)
                            resU = resU + round(param.avg);
                        else {
                            resU = resU + round(param.avg);
                            if (viewPointLineDelta)
                                resU = resU + ' (\u0394 ' + round(param.avg - prevVals[i]) + ')';
                        }
                        prevVals[i] = param.avg;
                    } else if (param.max !== undefined && param.min !== undefined)
                        resU = resU + round(param.min) + ' - ' + round(param.max);
                    resU = resU + '</span>';
                }
            }
            _plotLinesUpLabels[pid].attr({
                text: resU
            });
            _plotLinesDownLabels[pid].attr({
                text: resL
            });
        }
    }
}

function updatePositionLabels() {
    for (let i of Object.keys(_plotLines)) {
        if (chart.xAxis[0].toPixels(_plotLines[i].options.value) > chart.xAxis[0].left
            && chart.xAxis[0].toPixels(_plotLines[i].options.value) < chart.xAxis[0].left + chart.xAxis[0].len) {
            _plotLinesUpLabels[i].attr({
                x: chart.xAxis[0].toPixels(_plotLines[i].options.value),
                y: chart.plotTop,
                visibility: 'visible'
            });
            _plotLinesDownLabels[i].attr({
                x: chart.xAxis[0].toPixels(_plotLines[i].options.value),
                y: chart.plotTop + chart.plotHeight,
                visibility: 'visible'
            });
        } else {
            _plotLinesUpLabels[i].attr({
                visibility: 'hidden',
            });
            _plotLinesDownLabels[i].attr({
                visibility: 'hidden',
            });
        }
    }
}

//check and update PlotLines after update
function updatePlotLines() {
    for (let i of Object.keys(_plotLines)) {
        if (_plotLines[i].hasOwnProperty("options")) {
            if (_plotLines[i].options.value < chart.xAxis[0].dataMin || _plotLines[i].options.value > chart.xAxis[0].dataMax || chart.xAxis[0].options.type === "category") {
                _plotLines[i].destroy();
                delete _plotLines[i];
                _plotLinesUpLabels[i].destroy();
                delete _plotLinesUpLabels[i];
                _plotLinesDownLabels[i].destroy();
                delete _plotLinesDownLabels[i];
                delete pointData[i];
                delete sortPointID[i];
            } else {
                createPlotLinesLabels(_plotLines[i].options.value, _plotLines[i].id);
            }
        } else {
            delete _plotLines[i];
            chart.xAxis[0].removePlotLine(Number(i));
            _plotLinesUpLabels[i].destroy();
            delete _plotLinesUpLabels[i];
            _plotLinesDownLabels[i].destroy();
            delete _plotLinesDownLabels[i];
            delete pointData[i];
            delete sortPointID[i];
        }
    }

    updatePositionLabels();
}

function _hidePlotLines() {
    for (let i of Object.keys(_plotLines)) {
        if (isShowPlotLines) {
            _plotLines[i].svgElem.attr({
                visibility: 'visible'
            });
            _plotLinesUpLabels[i].attr({
                visibility: 'visible'
            });
            _plotLinesDownLabels[i].attr({
                visibility: 'visible'
            });
        } else {
            _plotLines[i].svgElem.attr({
                visibility: 'hidden'
            });
            _plotLinesUpLabels[i].attr({
                visibility: 'hidden',
            });
            _plotLinesDownLabels[i].attr({
                visibility: 'hidden',
            });
        }
    }
}

let isMovePlotLine = false;
let isRemoveCurPlotLine = false;
let curPlotLineID;
let curPlotLineX;
let newPlotLineX;

let start = function (e) {
    if (isMovePlotLine) {
        stop();
        return;
    }
    isMovePlotLine = true;
    curPlotLineID = Number(this.id);
    chart.options.chart.panning = false;
    addEventListener('mousemove', step);
    addEventListener('mouseup', stop);
    curPlotLineX = chart.xAxis[0].toPixels(_plotLines[curPlotLineID].options.value);
    newPlotLineX = e.pageX;
    chart.zoomType = 'xy';
    chart.pointer.zoomX = false;
    chart.pointer.zoomY = false;
    _plotLinesUpLabels[curPlotLineID].attr({
        zIndex: 6
    });
    _plotLinesDownLabels[curPlotLineID].attr({
        zIndex: 6
    });
    isRemoveCurPlotLine = false;
};

let step = function (e) {
    let left = chart.xAxis[0].left;
    isRemoveCurPlotLine = (e.pageX < left) || (e.pageX > left + chart.plotWidth);
    _plotLines[curPlotLineID].svgElem.translate(newPlotLineX - curPlotLineX);
    _plotLinesUpLabels[curPlotLineID].translate(newPlotLineX, chart.plotTop);
    _plotLinesDownLabels[curPlotLineID].translate(newPlotLineX, chart.plotTop + chart.plotHeight);
    newPlotLineX = e.pageX;
    createPlotLinesLabels(chart.pointer.getCoordinates(e).xAxis[0].value, curPlotLineID);
};

let stop = function () {
    removeEventListener('mousemove', step);
    removeEventListener('mouseup', stop);
    chart.xAxis[0].removePlotBand(curPlotLineID);
    if (!isRemoveCurPlotLine) {
        _plotLines[curPlotLineID] = chart.xAxis[0].addPlotLine({
            color: Highcharts.getOptions().tooltip.backgroundColor,
            id: curPlotLineID,
            label: "LINE",
            value: Math.round(chart.xAxis[0].toValue(newPlotLineX)),
            width: 2,
            zIndex: 6
        });
        _plotLines[curPlotLineID].svgElem.css({'cursor': 'ew-resize'}).attr({id: curPlotLineID}).on('mousedown', start);
        updatePositionLabels();
    } else {
        delete _plotLines[curPlotLineID];
        _plotLinesUpLabels[curPlotLineID].destroy();
        delete _plotLinesUpLabels[curPlotLineID];
        _plotLinesDownLabels[curPlotLineID].destroy();
        delete _plotLinesDownLabels[curPlotLineID];
        delete pointData[curPlotLineID];
        delete sortPointID[curPlotLineID];
    }
    chart.zoomType = 'xy';
    chart.pointer.zoomX = true;
    chart.pointer.zoomY = true;
    isMovePlotLine = false;
    isRemoveCurPlotLine = false;
    chart.options.chart.panning = true;
};

//functions for showing hints for buttons
var showPlotLinesLabel;
var createPlotLineLabel;
var groupsPIOXLabel;

function createHintLabels() {
    if (showPlotLinesLabel) showPlotLinesLabel.destroy();
    if (createPlotLineLabel) createPlotLineLabel.destroy();
    showPlotLinesLabel = chart.renderer.label(
        "", chart.plotSizeX + chart.plotLeft - 100, chart.plotTop + 30, 'rect', chart.plotSizeX + chart.plotLeft - 14, chart.plotTop - 50,
        false, false, "hint"
    ).attr({
        visibility: 'hidden', id: 'showPlotLinesLabel', zIndex: 9,
        padding: 8,
        fill: "var(--small-object-fill-color)",
    }).add();
    createPlotLineLabel = chart.renderer.label(
        "", chart.plotSizeX + chart.plotLeft - 180, chart.plotTop + 30, 'rect', chart.plotSizeX + chart.plotLeft - 37, chart.plotTop - 50,
        false, false, "hint"
    ).attr({
        visibility: 'hidden', id: 'createPlotLineLabel', zIndex: 9,
        padding: 8,
        fill: "var(--small-object-fill-color)",
    }).add();
    groupsPIOXLabel = chart.renderer.label(
        "", chart.plotSizeX + chart.plotLeft - 140, chart.plotTop + 65, 'rect', chart.plotSizeX + chart.plotLeft - 13, chart.plotTop - 45,
        false, false, "hint"
    ).attr({
        visibility: 'hidden', id: 'selectGroups', zIndex: 10,
        padding: 8,
        fill: "var(--small-object-fill-color)",
    }).add();
}

let showHintForCreate = function (e) {
    createPlotLineLabel.attr({
        text: e.type === 'mouseenter' ? (!mode ? "Включить создание метки" : "Выключить создание метки") : "",
        visibility: e.type === 'mouseleave' ? 'hidden' : 'visible',
        zIndex: 9,
    });
    hideTooltip();
};

let showHintForShow = function (e) {
    showPlotLinesLabel.attr({
        text: e.type === 'mouseenter' ? (isShowPlotLines ? "Показать метки" : "Скрыть метки") : "",
        visibility: e.type === 'mouseleave' ? 'hidden' : 'visible',
        zIndex: 9,
    });
    hideTooltip();
};

let showHintForGroups = function (e) {
    groupsPIOXLabel.attr({
        text: e.type === 'mouseenter' ? (isShowPIOXWindow ? "Окно распределения" : "Окно распределения") : "",
        visibility: e.type === 'mouseleave' ? 'hidden' : (isShowPIOXWindow ? "hidden" : "visible"),
        zIndex: 9,
    });
    hideTooltip();
}

function hideTooltip() {
    chart.tooltip.hide();
}

let precision = 1000;

function round(x) {
    return Math.round(x * precision) / precision;
}

let mode = !1;
function setMode(e) {
    if (!isShowPlotLines) {
        button.destroy();
        button = chart.renderer.button("", chart.plotSizeX + chart.plotLeft - 45, chart.plotTop + 5, setMode,
            mode ? buttonStyles[0] : buttonStyles[2], buttonStyles[1], buttonStyles[2], undefined, 'diamond')
            .attr({
                zIndex: 8, id: 'createPlotLineButton',
                'cursor': 'pointer'
            }).on('mouseenter', showHintForCreate).on('mouseleave', showHintForCreate).add();
        mode = !mode;
    }
}

let isShowPlotLines = false;
function showPlotLines(e) {
        showPlotLinesButton.destroy();
    showPlotLinesButton = chart.renderer.button("", chart.plotSizeX + chart.plotLeft - 20, chart.plotTop + 5, showPlotLines,
        !isShowPlotLines ? buttonStyles[0] : buttonStyles[2], buttonStyles[1], buttonStyles[2], undefined, 'rect')
        .attr({
            zIndex: 8, id: 'showPlotLinesButton',
            'cursor': 'pointer'
        }).on('mouseenter', showHintForShow).on('mouseleave', showHintForShow).add();
    //showPlotLinesButton.css({'cursor':'pointer'});
    mode = !isShowPlotLines ? !0 : !1;
    if (!isShowPlotLines) setMode();
    _hidePlotLines();
    isShowPlotLines = !isShowPlotLines;
}

let isShowForecasting = false;
function showForecastingWindow(e) {
    isShowForecasting = !isShowForecasting;
    document.getElementById('window-forecasting').style.display = isShowForecasting ? 'block' : 'none';
}

function updateButtonsPositions() {
    button.attr({
        x: chart.plotSizeX + chart.plotLeft - 45,
        y: chart.plotTop + 5
    });
    showPlotLinesButton.attr({
        x: chart.plotSizeX + chart.plotLeft - 20,
        y: chart.plotTop + 5
    });
    forecastingButton.attr({
        x: chart.plotSizeX + chart.plotLeft - 20,
        y: chart.plotTop + 75
    })
}

function updateSVG() {
    updateButtonsPositions();
    updatePlotLines();
    updatePositionLabels();
    createHintLabels();
    if (isAllPointsInOneX) updatePIOX();
}


function timerFunction(func, t) {
    var timer;
    if (timer) clearTimeout(timer);
    timer = setTimeout(func, 300, t)
}


var _zoomType = undefined;
var _ttt = true;
window.onmousemove = function (e) {
    if (isMovePlotLine) {
        chart.pointer.zoomX = false;
        chart.pointer.zoomY = false;
        chart.pointer.zoomHor = false;
        chart.pointer.zoomVert = false;
        return;
    }

    if (chart === undefined)
        return;

    if (!chart.mouseIsDown || chart.mouseDownX === undefined || chart.mouseDownY === undefined)
        return;
    var _dX = Math.abs(e.chartX - chart.mouseDownX);
    var _dY = Math.abs(e.chartY - chart.mouseDownY);

    let _newZoomType = (_dY < 1 || chart.chartHeight < 1 || _dX / _dY >= chart.chartHeight / chart.chartWidth) ? 'x' : 'y';

    if (_zoomType === _newZoomType)
        return;

    _zoomType = _newZoomType;

    chart.update({chart: {zoomType: _zoomType}});

    if (_zoomType === 'x') {

        chart.pointer.zoomX = true;
        chart.pointer.zoomY = false;
        chart.pointer.zoomHor = true;
        chart.pointer.zoomVert = false;

        if (_ttt && chart.pointer.selectionMarker !== undefined && chart.pointer.selectionMarker.element !== undefined) {
            chart.pointer.selectionMarker.element.setAttribute("y", chart.plotTop);
            chart.pointer.selectionMarker.element.setAttribute("height", chart.plotHeight);
            chart.pointer.selectionMarker.element.setAttribute("x", Math.min(e.chartX, chart.pointer.mouseDownX));
            chart.pointer.selectionMarker.element.setAttribute("width", Math.abs(e.chartX - chart.pointer.mouseDownX));
        }
    } else {

        chart.pointer.zoomX = false;
        chart.pointer.zoomY = true;
        chart.pointer.zoomHor = false;
        chart.pointer.zoomVert = true;

        if (_ttt && chart.pointer.selectionMarker !== undefined && chart.pointer.selectionMarker.element !== undefined) {
            chart.pointer.selectionMarker.element.setAttribute("x", chart.plotLeft);
            chart.pointer.selectionMarker.element.setAttribute("width", chart.plotWidth);
            chart.pointer.selectionMarker.element.setAttribute("y", Math.min(e.chartY, chart.pointer.mouseDownY));
            chart.pointer.selectionMarker.element.setAttribute("height", Math.abs(e.chartY - chart.pointer.mouseDownY));
        }
    }
    //console.log("onmousemove " + _zoomType + "|" + chart.pointer.selectionMarker);
};


window.ondblclick = function () {
    chart.zoom();
};

window.onresize = function () {
    var timer;
    if (timer) clearTimeout(timer);
    timer = setTimeout(updateSVG, 500);
};

window.onclick = function (e) {
    if ((e.target.getAttribute('class') === 'highcharts-root' || e.target.getAttribute('class') === 'highcharts-tracker-line'
        || e.target.getAttribute('class') === 'highcharts-markers' || e.target.getAttribute('class') === 'highcharts-area') &&
        chart.series.length !== 0 &&
        mode === true && e.chartX > chart.xAxis[0].left && e.chartX < chart.xAxis[0].left + chart.xAxis[0].len) {
        createPlotLines(e);
    }
};


function _debounce(func) {
    var timer;
    return function (event) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(func, 500, event);
    };
}
