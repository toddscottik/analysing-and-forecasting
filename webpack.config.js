var path = require('path');
const webpack = require("webpack");

module.exports = [{
    mode: 'development',
    // mode: 'production',
    entry: './timeseries.js',
    output: {
        filename: 'timeseriesAnalysis.js',
        path: path.resolve(__dirname, 'dist'),
    },

    plugins: [
        new webpack.ProvidePlugin({
            process: 'process/browser',
        })
    ],
},

{
    mode: 'development',
    // mode: 'production',
    entry: './auto_arima.js',
    output: {
        filename: 'ARIMA.js',
        path: path.resolve(__dirname, 'dist'),
    },
    experiments:{
        topLevelAwait: true
    },

    resolve: {
        fallback: {
            'fs':false,
            'os': false,
            'net':false,
            'path':false
        }
    }
}]