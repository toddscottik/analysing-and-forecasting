(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){


var _skip = false;
//Автоматическое переподключение после восстановления связи
(function () {
    setInterval(function () {
            var e = document.getElementsByClassName("v-Notification-system v-position-top v-position-center");
            let desynch_base_clear = null;
            if(_currentTheme != null)
            {
                let mv_base_clear = document.querySelector('[href*="./VAADIN/themes/mv_base_clear/styles.css"]');
                if(mv_base_clear == null)
                    desynch_base_clear = document.querySelector('[href*="/VAADIN/themes/mv_base_clear/styles.css"]');
            }

            if (desynch_base_clear != null ||
                (!_skip && e.length > 0 &&  (e[0].textContent.indexOf("Session Expired") == 0 ||
                    e[0].textContent.indexOf("Communication problem") == 0 ||
                    e[0].textContent.indexOf("Internal error") == 0)))
                location.reload(true);
        },
        1000);

        setInterval(function () {
            if(window.MV_OnPing != null)
            {
                window.MV_OnPing();
            }
        }, 5000);
}
());

/*
(function () {
    if(window.MV_OnLoad == null)
    {
        let _onLoadInterval =  setInterval(function() {
            if(window.MV_OnLoad != null)
            {
                MV_OnLoad();
                clearInterval(_onLoadInterval);
            }
        }, 100);
    }
    else
    {
        MV_OnLoad();
    }


}
());

*/

let _currentTheme = null;
let _themeChanged = false;
let _newThemeLink = null;


var _skip2 = false;

function MV_ChangeTheme(theme)
{
    if(_currentTheme === theme)
        return;

    if(_skip2)
        return;


    _themeChanged = true;
    let mv_theme = null;
    //mv_theme.href =  "./VAADIN/themes/" + _curTheme + "/styles.css";

    if(_newThemeLink != null)
    {
        _newThemeLink.onload = null;
        mv_theme = _newThemeLink;
    }
    else
    {
        mv_theme = document.querySelector('[mv_theme]');
    }

    _newThemeLink = document.createElement("link");
    _newThemeLink.rel = "stylesheet";
    _newThemeLink.type = "text/css";
    _newThemeLink.href =  "./VAADIN/themes/" + theme + "/styles.css";
    _newThemeLink.onload = function ()
    {
        document.head.removeChild(mv_theme);

        let mv_base_clear = document.querySelector('[href*="./VAADIN/themes/mv_base_clear/styles.css"]');
        if(mv_base_clear != null)
            document.head.removeChild(mv_base_clear);

        _currentTheme = theme;
        _themeChanged = false;
        OnThemeChanged();
    };
    document.head.insertBefore(_newThemeLink, mv_theme.nextSibling);
    _newThemeLink.setAttribute("mv_theme", '');

    /*
    let links = document.getElementsByTagName("link");

    let styleLink;
    for(let i = 0; i < links.length; i++)
    {
        if(links[i].href.indexOf("VAADIN/themes") >= 0)
        {
            if(links[i].relList.contains("icon"))
            {
                links[i].href = "./VAADIN/themes/" + theme + "/favicon.ico";
            }
            else
            {
                styleLink = links[i];
            }
        }
    }

    */
}
function MV_CopyToClipboard(isMultiline, str)
{
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
        document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }

    MV_OnCopyToClipboard(isMultiline, str);
}


function OnThemeChanged()
{
    ContentLayoutPanel.UpdateClient(ContentLayoutPanel.mainPanel);
}


var shiftKey = false;
var altKey = false;
var ctrlKey = false;

function MV_OnUpdateKeys(e)
{
    if(typeof MV_StateKeyDown === "undefined")
        return;

    if(shiftKey !== e.shiftKey)
    {
        shiftKey = e.shiftKey;
        MV_StateKeyDown("shift", shiftKey);
        console.log("shiftKey " + shiftKey);
    }

    if(altKey !== e.altKey)
    {
        altKey = e.altKey;
        MV_StateKeyDown("alt", altKey);
        console.log("altKey " + altKey);
    }

    if(ctrlKey !== e.ctrlKey)
    {
        ctrlKey = e.ctrlKey;
        MV_StateKeyDown("ctrl", ctrlKey);
        console.log("ctrlKey " + ctrlKey);
    }
};
/*
var _lost = function()
{
    if(typeof MV_StateKeyDown === "undefined")
        return;

    _updateKeys({
        shiftKey: false,
        altKey: false,
        ctrlKey: false
    });
};
*/
window.onkeydown = MV_OnUpdateKeys;
window.onkeyup = MV_OnUpdateKeys;
//window.onclick = _updateKeys;
//window.onfocus = _lost;
//window.onblur = _lost;

var Mouse =
{
    clientX: 0,
    clientY: 0,
    pageX: 0,
    pageY: 0,
};


window.addEventListener('mousemove', function(event) {
    _mouseMove(event, false);
}, true);

window.addEventListener('drag', function(event) {
    event.dataTransfer.effectAllowed = 'all';
    MV_OnUpdateKeys(event);
    _mouseMove(event, true);
    ContentLayoutPanel.FindDropPanel();
}, true);


let _hideDraggingCurtain = false;

window.addEventListener('mousedown', function(event) {
    document.getElementsByTagName("body")[0].classList.add("i-hide-draggingCurtain");//Заплатка против бага, когда завеса появляется, без движения курсора
    _hideDraggingCurtain = true;
}, true);


function _mouseMove(event, isDrag)
{
    if(!isDrag && event.movementX == 0 && event.movementY == 0)
        return;

    var eventDoc, doc, body;

    event = event || window.event;

    if(_hideDraggingCurtain)
    {
        document.getElementsByTagName("body")[0].classList.remove("i-hide-draggingCurtain");
        _hideDraggingCurtain = false;
    }

    Mouse.pageX = 0;
    Mouse.pageY = 0;

    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        Mouse.pageX = event.clientX +
            (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
            (doc && doc.clientLeft || body && body.clientLeft || 0);
        Mouse.pageY = event.clientY +
            (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
            (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }
    else
    {
        Mouse.pageX = event.pageX;
        Mouse.pageY = event.pageY;
    }

    Mouse.clientX = event.clientX;
    Mouse.clientY = event.clientY;

    //MV_MouseState(Mouse.pageX, Mouse.pageY, Mouse.clientX, Mouse.clientY);
}

//function MV_MouseState() {}




function MV_SetScrollPosition(id, scrollLeft, scrollTop)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    scrollH.scrollLeft = scrollLeft;
    scrollV.scrollTop = scrollTop;
}
function MV_RequestScrollPosition(id)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    if(scrollH.onscroll != null || scrollV.onscroll != null)
        return;

    var _fireUpdateScrollPos = function()
    {
        _updateScrollPos(id, scrollH, scrollV);
    };

    scrollH.onscroll = _fireUpdateScrollPos;
    scrollV.onscroll = _fireUpdateScrollPos;

    _updateScrollPos(id, scrollH, scrollV);
}

function MV_StopRequestScrollPosition(id)
{
    var scrollH = document.querySelector("#" + id + " .v-treegrid-scroller-horizontal");
    var scrollV = document.querySelector("#" + id + " .v-treegrid-scroller-vertical");

    scrollH.onscroll = null;
    scrollV.onscroll = null;
}


function _updateScrollPos(id, scrollH, scrollV)
{
    MV_RequestScrollPositionListener(
        id,
        {
            scrollLeft:     scrollH.scrollLeft,
            scrollTop:      scrollV.scrollTop
        });
}




function MV_RequestBoundingClientRect(id, isRelative)
{
    var obj = document.getElementById(id);
    var rect = obj.getBoundingClientRect();

    var scrollX = isRelative ? window.scrollX : 0;
    var scrollY = isRelative ? window.scrollY : 0;

    MV_RequestBoundingListener(
        id,
        {
            m_Left:     rect.left   + scrollX,
            m_Right:    rect.right  + scrollX,
            m_Bottom:   rect.bottom + scrollY,
            m_Top:      rect.top    + scrollY,

            m_Height:   rect.height,
            m_Width:    rect.width,

            m_X:        rect.x + scrollX,
            m_Y:        rect.y + scrollY
        });
}



var ContentLayoutPanel = {
    contentLayoutPanel: null,
    mainPanel: null,
    splitPanels: null,
    focusPanel: null,
    targetPosition: null,
    isDragged: false,
    isInit: -1,
    draggedSplitter: null,
    mainStyleSheet: null,
    UpdateClient: function (mainPanel)
    {

        if(this.isInit === -1)
            this.isInit = 0;

        mainPanel.parent = null;
        mainPanel.index = -1;

        this.mainPanel = mainPanel;
        this.splitPanels = {};
        this.focusPanel = null;

        if(this.isInit === 0)
        {
            setTimeout('ContentLayoutPanel._init()',0);
        }
        else
        {
            setTimeout('ContentLayoutPanel._doParceUpdate()',0);
        }
    },
    _doParceUpdate:function()
    {
        this._parsePanel(this.mainPanel);
        this.Update(this.mainPanel);
    },
    _init:function()
    {
        this.mainStyleSheet =  document.getElementsByTagName("style")[0].sheet;

        this.contentLayoutPanel = function()
        {
            return document.getElementById("ContentLayoutPanel");
        };


        window.addEventListener("resize", _debounce(function(e){
            ContentLayoutPanel.Update(ContentLayoutPanel.mainPanel);
            //_update();
        }));

        window.addEventListener("mouseup", function(event) {
            if(ContentLayoutPanel.draggedSplitter != null)
            {
                if(ContentLayoutPanel.draggedSplitter.parentPanel.isDirty)
                    ContentLayoutPanel.Update(ContentLayoutPanel.draggedSplitter.parentPanel);

                ContentLayoutPanel.contentLayoutPanel().setAttribute("state", "");
                ContentLayoutPanel.draggedSplitter = null;
            }

        },true);

        window.addEventListener("mousedown", function(event)
        {
            if(ContentLayoutPanel.draggedSplitter != null)
                return;

            var _splitter = event.target;
            if(_splitter.classList.contains("v-splitpanel-hsplitter") || _splitter.classList.contains("v-splitpanel-vsplitter"))
            {
                var _splitPanel = _splitter.parentElement.parentElement;

                ContentLayoutPanel.draggedSplitter = ContentLayoutPanel.splitPanels[_splitPanel.id];
            }
            else
            {
                ContentLayoutPanel.draggedSplitter = null;
            }

            if(ContentLayoutPanel.draggedSplitter != null)
                ContentLayoutPanel.contentLayoutPanel().setAttribute("state", "active");

            //console.log("mousedown")
        }, true);

        window.addEventListener("mousemove", function(event)
        {
            if(ContentLayoutPanel.draggedSplitter == null)
                return;

            if(ContentLayoutPanel._moveSplitter(ContentLayoutPanel.draggedSplitter, null))
            {
                ContentLayoutPanel.draggedSplitter.parentPanel.isDirty = true;
                //ContentLayoutPanel.Update(ContentLayoutPanel.draggedSplitter.parentPanel);
            }

            //ContentLayoutPanel.Update([_s.firstPanel, _s.secondPanel]);

        }, true);

        this.isInit = 1;

        ContentLayoutPanel._doParceUpdate();
    },
    _updateSplitPanels: function(splitPanel, startPanelIndex, endPanelIndex, curIndex, direction)
    {
        if(startPanelIndex > endPanelIndex)
            return;

        var _s = splitPanel.splits[startPanelIndex];
        this._updateSplitPanels(_s.firstPanel, 0, _s.firstPanel.splits.length - 1, -1, direction);

        for(var i = startPanelIndex; i <= endPanelIndex; i++)
        {
            _s = splitPanel.splits[i];


            if(splitPanel.direction === direction && i != curIndex)
            {
                var _splitPanelWrapper = _s.GetSplitPanelWrapper();
                var _splitter = _s.GetSplitterElement();

                if(splitPanel.direction === "Horizontal")
                {
                    _splitter.style.left =  Math.round(_splitPanelWrapper.offsetWidth * _s.position) + "px";
                }
                else
                {
                    _splitter.style.top =  Math.round(_splitPanelWrapper.offsetHeight * _s.position) + "px";
                }

            }
            this._updateSplitPanels(_s.secondPanel, 0, _s.secondPanel.splits.length - 1, -1, direction);
        }
    },
    _moveSplitter: function(_split, position)
    {

        var _splitter = _split.GetSplitterElement();
        var _splitPanelWrapper = _split.GetSplitPanelWrapper();

        var _firstElement = _split.firstPanel.GetElement().parentElement;
        var _secondElement = _split.secondPanel.GetElement().parentElement;

        //var _parent = _split.parentPanel.GetElement();

        if(position != null)
        {
            _split.position = Math.round(position * 1000) / 1000;
        }

        if(_split.parentPanel.direction === "Horizontal")
        {
            if(position == null)
            {
                var _width = Math.max(1, _splitPanelWrapper.offsetWidth);

                var newPosition = Math.round((_splitter.offsetLeft + _splitter.offsetWidth * 0.5) / _width * 1000) / 1000;
                if(newPosition === _split.position)
                    return false;

                _split.position = newPosition
            }

            var _pos = "(100% - (" + _splitPanelWrapper.style.left + " + " + _splitPanelWrapper.style.right + ")) * " + _split.position + " + " + _splitPanelWrapper.style.left;

            if(_split.index === 0)
                _firstElement.style.left = "0px";
            if(_split.index === _split.parentPanel.splits.length - 1)
                _secondElement.style.right = "0px";

            _firstElement.style.right = "calc(100% - (" + _pos + "))";
            _secondElement.style.left = "calc(" + _pos + ")";
        }
        else
        {
            if(position == null)
            {
                var _height = Math.max(1, _splitPanelWrapper.offsetHeight);
                var newPosition = Math.round((_splitter.offsetTop + _splitter.offsetHeight * 0.5) / _height * 1000) / 1000;
                if(newPosition === _split.position)
                    return false;

                _split.position = newPosition
            }


            var _pos = "(100% - (" + _splitPanelWrapper.style.top + " + " + _splitPanelWrapper.style.bottom + ")) * " + _split.position + " + " + _splitPanelWrapper.style.top ;

            if(_split.index === 0)
                _firstElement.style.top = "0px";
            if(_split.index === _split.parentPanel.splits.length - 1)
                _secondElement.style.bottom = "0px";

            _firstElement.style.bottom = "calc(100% - (" + _pos + "))";
            _secondElement.style.top = "calc(" + _pos + ")";
        }


        if(position == null)
        {
            var startPanelIndex = _split.index;
            var endPanelIndex = _split.index;

            for(var i = 0; i < _split.parentPanel.splits.length; i++)
            {
                var _s = _split.parentPanel.splits[i];

                if(i < _split.index)
                {
                    if(_s.position > _split.position)
                    {
                        this._moveSplitter(_s, _split.position);

                        if(i < startPanelIndex)
                            startPanelIndex = i;
                    }
                }
                else if(i > _split.index)
                {
                    if(_s.position < _split.position)
                    {
                        this._moveSplitter(_s, _split.position);

                        if(i > endPanelIndex)
                            endPanelIndex = i;
                    }
                }
            }

            this._updateSplitPanels(_split.parentPanel, startPanelIndex, endPanelIndex, _split.index, _split.parentPanel.direction);
        }

        return true;
    },
    _parsePanel: function(curPanel)
    {
        /*
        curPanel.GetElement = function(updateCache)
        {
            return this._cacheElement == null ? document.getElementById(this.id) : this._cacheElement;
        };

        curPanel.GetFormElement = function()
        {
            return this.form == null ? null : document.getElementById(this.form);
        };*/

        curPanel.GetElement = function()
        {
            return document.getElementById(this.id);
        };

        curPanel.GetFormElement = function()
        {
            return document.getElementById(this.form);
        };


        var panelStyle = this._getStyleRule(".i-DropPanel", curPanel.id);

        panelStyle.setProperty("min-width", curPanel.minWidth + "px", 'important');
        panelStyle.setProperty("min-height", curPanel.minHeight + "px", 'important');


        //sheet.$$("cells").getItem(3)[2]

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            var _s = curPanel.splits[i];

            _s.firstPanel = curPanel.childs[i];
            _s.secondPanel = curPanel.childs[i + 1];
            _s.parentPanel = curPanel;
            _s.index = i;

            _s.GetSplitterElement = function()
            {
                return document.getElementById(this.id).childNodes[0].childNodes[1];
            };

            _s.GetSplitPanelWrapper = function()
            {
                return document.getElementById(this.id).parentElement;
            };

            this.splitPanels[_s.id] = _s;
        }

        for(var i = 0; i < curPanel.childs.length; i++)
        {
            curPanel.childs[i].parent = curPanel;
            curPanel.childs[i].index = i;

            this._parsePanel(curPanel.childs[i]);
        }

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            var _s = curPanel.splits[i];

            //_s.firstPanel.GetElement(true);
            //_s.secondPanel.GetElement(true);

            ContentLayoutPanel._moveSplitter(_s, _s.position);
        }
    },
    Update: function(panel)
    {
        if(this.isInit !== 1)
            return;

        //console.log("Update")

        var panelTransforms = {};

        ContentLayoutPanel._updatePanel(panel, panelTransforms);
        MV_ContentLayoutPanel_UpdateServer(panelTransforms);


        panel.isDirty = false;
    },
    _updatePanel: function(curPanel, panelTransforms)
    {
        if(curPanel == null)
            return;

        var rect = curPanel.GetElement();
        var form = curPanel.GetFormElement();

        if(rect == null)
        {
            var gg = "";
            gg += "we";

            return;
        }

        var bRect = rect.getBoundingClientRect();


        var _left = Math.round(bRect.left);
        var _top = Math.round(bRect.top);
        var _width = Math.round(bRect.width);
        var _height = Math.round(bRect.height);

        var _splitsPositions = [];

        for(var i = 0; i < curPanel.splits.length; i++)
        {
            _splitsPositions.push(curPanel.splits[i].position);
        }

        panelTransforms[curPanel.id] = {
            left: _left,
            top: _top,
            width: _width,
            height: _height,
            splitsPositions: _splitsPositions
        };

        if(form != null/* && form.classList.contains("i-is-embedded")*/)
        {
            form.style.left = _left + "px";
            form.style.top = _top + "px";
            form.style.width = _width + "px";
            form.style.height = _height + "px";
        }

        for(var i = 0; i < curPanel.childs.length; i++)
        {
            this._updatePanel(curPanel.childs[i], panelTransforms);
        }
    },
    _getStyleRule: function(selector, id)
    {
        var styleRule = null;
        for(var i = 0; i < this.mainStyleSheet.rules.length; i++)
        {
            if(this.mainStyleSheet.rules[i].selectorText.indexOf(selector + "#" + id) >= 0)
            {
                styleRule = this.mainStyleSheet.rules[i];
                break;
            }
        }
        if(styleRule == null)
            styleRule = this.mainStyleSheet.rules[this.mainStyleSheet.insertRule(selector + "#" + id + "{}")];

        return styleRule.style;
    },
    Start: function ()
    {
        if(this.isInit !== 1)
            return;

        this.isDragged = true;
    },
    Release: function ()
    {
        if(this.isInit !== 1 || !this.isDragged)
            return;

        this.isDragged = false;

        if(this.focusPanel == null)
        {
            MV_OnReleaseContentLayoutPanel(null, null);
            return;
        }

        MV_OnReleaseContentLayoutPanel(this.focusPanel.id, this.targetPosition);

        this.focusPanel.GetElement().setAttribute("active", null);

        this.focusPanel = null;
    },
    FindDropPanel: function ()
    {
        if(this.isInit !== 1 || !this.isDragged)
            return;

        var focusPanel = this._findFocusPanel(this.mainPanel);
        var targetPosition = focusPanel == null ? null : this._calcTargetPosition(focusPanel, focusPanel.GetElement().getBoundingClientRect());

        while(!(focusPanel == null ||
                targetPosition == null ||
                targetPosition === "center" || targetPosition === "left" || targetPosition === "right" || targetPosition === "top" || targetPosition === "bottom"))
        {
            if(focusPanel === this.mainPanel)
            {
                if(targetPosition === "top_left" || targetPosition === "top_right")
                    targetPosition = "top";
                else if(targetPosition === "left_top" || targetPosition === "left_bottom")
                    targetPosition = "left";
                else if(targetPosition === "right_top" || targetPosition === "right_bottom")
                    targetPosition = "right";
                else if(targetPosition === "bottom_left" || targetPosition === "bottom_right")
                    targetPosition = "bottom";

                break;
            }

            if(focusPanel.direction === "Horizontal")
            {
                if(focusPanel.index === 0 &&
                    (targetPosition === "left_top" || targetPosition === "top_left" || targetPosition === "right_top" || targetPosition === "top_right"))
                {
                }
                else if(focusPanel.index === (focusPanel.parent.childs.length - 1) &&
                    (targetPosition === "left_bottom" || targetPosition === "bottom_left" || targetPosition === "right_bottom" || targetPosition === "bottom_right"))
                {
                }
                else
                {
                    if(targetPosition === "left_top" || targetPosition === "top_left" ||
                        targetPosition === "left_bottom" || targetPosition === "bottom_left")
                    {
                        targetPosition = "left";
                    }
                    else if(targetPosition === "right_bottom" || targetPosition === "bottom_right" ||
                        targetPosition === "right_top" || targetPosition === "top_right")
                    {
                        targetPosition = "right";
                    }

                    //break;
                }
            }
            else if(focusPanel.direction === "Vertical")
            {
                if(focusPanel.index === 0 &&
                    (targetPosition === "top_left" || targetPosition === "left_top" || targetPosition === "bottom_left" || targetPosition === "left_bottom"))
                {
                }
                else if(focusPanel.index === (focusPanel.parent.childs.length - 1) &&
                    (targetPosition === "top_right" || targetPosition === "right_top" || targetPosition === "bottom_right" || targetPosition === "right_bottom"))
                {
                }
                else
                {
                    if(targetPosition === "top_right" || targetPosition === "right_top" ||
                        targetPosition === "top_left" || targetPosition === "left_top")
                    {
                        targetPosition = "top";
                    }
                    else if(targetPosition === "bottom_right" || targetPosition === "right_bottom" ||
                        targetPosition === "bottom_left" || targetPosition === "left_bottom")
                    {
                        targetPosition = "bottom";
                    }

                    //break;
                }
            }

            focusPanel = focusPanel.parent;
        }

        if(this.focusPanel != null && this.focusPanel !== focusPanel)
            this.focusPanel.GetElement().setAttribute("active", null);

        this.focusPanel = focusPanel;

        if(focusPanel != null)
        {
            this.targetPosition = targetPosition;
            this.focusPanel.GetElement().setAttribute("active", this.targetPosition);
        }
        else
        {
            this.targetPosition = null;
        }

    },
    _findFocusPanel: function (curPanel)
    {
        var bRect = curPanel.GetElement().getBoundingClientRect();

        if(Mouse.clientX >= bRect.x && Mouse.clientX <= bRect.x + bRect.width &&
            Mouse.clientY >= bRect.y && Mouse.clientY <= bRect.y + bRect.height)
        {
            for(var i = 0; i < curPanel.childs.length; i++)
            {
                var panel = this._findFocusPanel(curPanel.childs[i]);
                if(panel != null)
                    return panel;
            }
            return curPanel;
        }

        return null;
    },
    _calcTargetPosition: function (focusPanel, bRect) {
        var maxBorderWidth  = Math.min(200, bRect.width * 0.25);
        var maxBorderHeight = Math.min(200, bRect.height * 0.25);


        if(focusPanel === this.mainPanel && focusPanel.form == null)
            return "center";

        if( Math.abs(Mouse.clientX - (bRect.x + bRect.width / 2)) <=  Math.max(bRect.width * 0.25, (bRect.width * 0.5 - maxBorderWidth)) &&
            Math.abs(Mouse.clientY - (bRect.y + bRect.height / 2)) <=  Math.max(bRect.height * 0.25, (bRect.height * 0.5 - maxBorderHeight)))
        {
            return "center";
        }

        var leftRange = Mouse.clientX - bRect.x;
        var rightRange = bRect.x + bRect.width - Mouse.clientX;

        var topRange = Mouse.clientY - bRect.y;
        var bottomRange = bRect.y + bRect.height - Mouse.clientY;

        if(leftRange <= maxBorderWidth)
        {
            if(topRange <= maxBorderHeight)
            {
                if(leftRange < topRange)
                {
                    return "left_top";
                }
                else
                {
                    return "top_left";
                }
            }
            else if(bottomRange <= maxBorderHeight)
            {
                if(leftRange < bottomRange)
                {
                    return "left_bottom";
                }
                else
                {
                    return "bottom_left";
                }
            }
            else
            {
                return "left";
            }
        }
        else if(rightRange <= maxBorderWidth)
        {
            if(topRange <= maxBorderHeight)
            {
                if(rightRange < topRange)
                {
                    return "right_top";
                }
                else
                {
                    return "top_right";
                }
            }
            else if(bottomRange <= maxBorderHeight)
            {
                if(rightRange < bottomRange)
                {
                    return "right_bottom";
                }
                else
                {
                    return "bottom_right";
                }
            }
            else
            {
                return "right";
            }
        }
        else if(topRange <= maxBorderHeight)
        {
            return "top";
        }
        else if(bottomRange <= maxBorderHeight)
        {
            return "bottom";
        }

        return null;
    }
};


function _debounce(func){
    var timer;
    return function(event){
        if(timer) clearTimeout(timer);
        timer = setTimeout(func,500,event);
    };
};

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Vzci9sb2NhbC9saWIvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsImpzL21haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCJcblxudmFyIF9za2lwID0gZmFsc2U7XG4vL9CQ0LLRgtC+0LzQsNGC0LjRh9C10YHQutC+0LUg0L/QtdGA0LXQv9C+0LTQutC70Y7Rh9C10L3QuNC1INC/0L7RgdC70LUg0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC90LjRjyDRgdCy0Y/Qt9C4XG4oZnVuY3Rpb24gKCkge1xuICAgIHNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBlID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInYtTm90aWZpY2F0aW9uLXN5c3RlbSB2LXBvc2l0aW9uLXRvcCB2LXBvc2l0aW9uLWNlbnRlclwiKTtcbiAgICAgICAgICAgIGxldCBkZXN5bmNoX2Jhc2VfY2xlYXIgPSBudWxsO1xuICAgICAgICAgICAgaWYoX2N1cnJlbnRUaGVtZSAhPSBudWxsKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxldCBtdl9iYXNlX2NsZWFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignW2hyZWYqPVwiLi9WQUFESU4vdGhlbWVzL212X2Jhc2VfY2xlYXIvc3R5bGVzLmNzc1wiXScpO1xuICAgICAgICAgICAgICAgIGlmKG12X2Jhc2VfY2xlYXIgPT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgZGVzeW5jaF9iYXNlX2NsZWFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignW2hyZWYqPVwiL1ZBQURJTi90aGVtZXMvbXZfYmFzZV9jbGVhci9zdHlsZXMuY3NzXCJdJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChkZXN5bmNoX2Jhc2VfY2xlYXIgIT0gbnVsbCB8fFxuICAgICAgICAgICAgICAgICghX3NraXAgJiYgZS5sZW5ndGggPiAwICYmICAoZVswXS50ZXh0Q29udGVudC5pbmRleE9mKFwiU2Vzc2lvbiBFeHBpcmVkXCIpID09IDAgfHxcbiAgICAgICAgICAgICAgICAgICAgZVswXS50ZXh0Q29udGVudC5pbmRleE9mKFwiQ29tbXVuaWNhdGlvbiBwcm9ibGVtXCIpID09IDAgfHxcbiAgICAgICAgICAgICAgICAgICAgZVswXS50ZXh0Q29udGVudC5pbmRleE9mKFwiSW50ZXJuYWwgZXJyb3JcIikgPT0gMCkpKVxuICAgICAgICAgICAgICAgIGxvY2F0aW9uLnJlbG9hZCh0cnVlKTtcbiAgICAgICAgfSxcbiAgICAgICAgMTAwMCk7XG5cbiAgICAgICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYod2luZG93Lk1WX09uUGluZyAhPSBudWxsKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5NVl9PblBpbmcoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgNTAwMCk7XG59XG4oKSk7XG5cbi8qXG4oZnVuY3Rpb24gKCkge1xuICAgIGlmKHdpbmRvdy5NVl9PbkxvYWQgPT0gbnVsbClcbiAgICB7XG4gICAgICAgIGxldCBfb25Mb2FkSW50ZXJ2YWwgPSAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZih3aW5kb3cuTVZfT25Mb2FkICE9IG51bGwpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgTVZfT25Mb2FkKCk7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChfb25Mb2FkSW50ZXJ2YWwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCAxMDApO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgICBNVl9PbkxvYWQoKTtcbiAgICB9XG5cblxufVxuKCkpO1xuXG4qL1xuXG5sZXQgX2N1cnJlbnRUaGVtZSA9IG51bGw7XG5sZXQgX3RoZW1lQ2hhbmdlZCA9IGZhbHNlO1xubGV0IF9uZXdUaGVtZUxpbmsgPSBudWxsO1xuXG5cbnZhciBfc2tpcDIgPSBmYWxzZTtcblxuZnVuY3Rpb24gTVZfQ2hhbmdlVGhlbWUodGhlbWUpXG57XG4gICAgaWYoX2N1cnJlbnRUaGVtZSA9PT0gdGhlbWUpXG4gICAgICAgIHJldHVybjtcblxuICAgIGlmKF9za2lwMilcbiAgICAgICAgcmV0dXJuO1xuXG5cbiAgICBfdGhlbWVDaGFuZ2VkID0gdHJ1ZTtcbiAgICBsZXQgbXZfdGhlbWUgPSBudWxsO1xuICAgIC8vbXZfdGhlbWUuaHJlZiA9ICBcIi4vVkFBRElOL3RoZW1lcy9cIiArIF9jdXJUaGVtZSArIFwiL3N0eWxlcy5jc3NcIjtcblxuICAgIGlmKF9uZXdUaGVtZUxpbmsgIT0gbnVsbClcbiAgICB7XG4gICAgICAgIF9uZXdUaGVtZUxpbmsub25sb2FkID0gbnVsbDtcbiAgICAgICAgbXZfdGhlbWUgPSBfbmV3VGhlbWVMaW5rO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgICBtdl90aGVtZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1ttdl90aGVtZV0nKTtcbiAgICB9XG5cbiAgICBfbmV3VGhlbWVMaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxpbmtcIik7XG4gICAgX25ld1RoZW1lTGluay5yZWwgPSBcInN0eWxlc2hlZXRcIjtcbiAgICBfbmV3VGhlbWVMaW5rLnR5cGUgPSBcInRleHQvY3NzXCI7XG4gICAgX25ld1RoZW1lTGluay5ocmVmID0gIFwiLi9WQUFESU4vdGhlbWVzL1wiICsgdGhlbWUgKyBcIi9zdHlsZXMuY3NzXCI7XG4gICAgX25ld1RoZW1lTGluay5vbmxvYWQgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgZG9jdW1lbnQuaGVhZC5yZW1vdmVDaGlsZChtdl90aGVtZSk7XG5cbiAgICAgICAgbGV0IG12X2Jhc2VfY2xlYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbaHJlZio9XCIuL1ZBQURJTi90aGVtZXMvbXZfYmFzZV9jbGVhci9zdHlsZXMuY3NzXCJdJyk7XG4gICAgICAgIGlmKG12X2Jhc2VfY2xlYXIgIT0gbnVsbClcbiAgICAgICAgICAgIGRvY3VtZW50LmhlYWQucmVtb3ZlQ2hpbGQobXZfYmFzZV9jbGVhcik7XG5cbiAgICAgICAgX2N1cnJlbnRUaGVtZSA9IHRoZW1lO1xuICAgICAgICBfdGhlbWVDaGFuZ2VkID0gZmFsc2U7XG4gICAgICAgIE9uVGhlbWVDaGFuZ2VkKCk7XG4gICAgfTtcbiAgICBkb2N1bWVudC5oZWFkLmluc2VydEJlZm9yZShfbmV3VGhlbWVMaW5rLCBtdl90aGVtZS5uZXh0U2libGluZyk7XG4gICAgX25ld1RoZW1lTGluay5zZXRBdHRyaWJ1dGUoXCJtdl90aGVtZVwiLCAnJyk7XG5cbiAgICAvKlxuICAgIGxldCBsaW5rcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwibGlua1wiKTtcblxuICAgIGxldCBzdHlsZUxpbms7XG4gICAgZm9yKGxldCBpID0gMDsgaSA8IGxpbmtzLmxlbmd0aDsgaSsrKVxuICAgIHtcbiAgICAgICAgaWYobGlua3NbaV0uaHJlZi5pbmRleE9mKFwiVkFBRElOL3RoZW1lc1wiKSA+PSAwKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZihsaW5rc1tpXS5yZWxMaXN0LmNvbnRhaW5zKFwiaWNvblwiKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsaW5rc1tpXS5ocmVmID0gXCIuL1ZBQURJTi90aGVtZXMvXCIgKyB0aGVtZSArIFwiL2Zhdmljb24uaWNvXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc3R5bGVMaW5rID0gbGlua3NbaV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAqL1xufVxuZnVuY3Rpb24gTVZfQ29weVRvQ2xpcGJvYXJkKGlzTXVsdGlsaW5lLCBzdHIpXG57XG4gICAgY29uc3QgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZXh0YXJlYScpO1xuICAgIGVsLnZhbHVlID0gc3RyO1xuICAgIGVsLnNldEF0dHJpYnV0ZSgncmVhZG9ubHknLCAnJyk7XG4gICAgZWwuc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xuICAgIGVsLnN0eWxlLmxlZnQgPSAnLTk5OTlweCc7XG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChlbCk7XG4gICAgY29uc3Qgc2VsZWN0ZWQgPVxuICAgICAgICBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKS5yYW5nZUNvdW50ID4gMFxuICAgICAgICAgICAgPyBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKS5nZXRSYW5nZUF0KDApXG4gICAgICAgICAgICA6IGZhbHNlO1xuICAgIGVsLnNlbGVjdCgpO1xuICAgIGRvY3VtZW50LmV4ZWNDb21tYW5kKCdjb3B5Jyk7XG4gICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChlbCk7XG4gICAgaWYgKHNlbGVjdGVkKSB7XG4gICAgICAgIGRvY3VtZW50LmdldFNlbGVjdGlvbigpLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgICAgICBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKS5hZGRSYW5nZShzZWxlY3RlZCk7XG4gICAgfVxuXG4gICAgTVZfT25Db3B5VG9DbGlwYm9hcmQoaXNNdWx0aWxpbmUsIHN0cik7XG59XG5cblxuZnVuY3Rpb24gT25UaGVtZUNoYW5nZWQoKVxue1xuICAgIENvbnRlbnRMYXlvdXRQYW5lbC5VcGRhdGVDbGllbnQoQ29udGVudExheW91dFBhbmVsLm1haW5QYW5lbCk7XG59XG5cblxudmFyIHNoaWZ0S2V5ID0gZmFsc2U7XG52YXIgYWx0S2V5ID0gZmFsc2U7XG52YXIgY3RybEtleSA9IGZhbHNlO1xuXG5mdW5jdGlvbiBNVl9PblVwZGF0ZUtleXMoZSlcbntcbiAgICBpZih0eXBlb2YgTVZfU3RhdGVLZXlEb3duID09PSBcInVuZGVmaW5lZFwiKVxuICAgICAgICByZXR1cm47XG5cbiAgICBpZihzaGlmdEtleSAhPT0gZS5zaGlmdEtleSlcbiAgICB7XG4gICAgICAgIHNoaWZ0S2V5ID0gZS5zaGlmdEtleTtcbiAgICAgICAgTVZfU3RhdGVLZXlEb3duKFwic2hpZnRcIiwgc2hpZnRLZXkpO1xuICAgICAgICBjb25zb2xlLmxvZyhcInNoaWZ0S2V5IFwiICsgc2hpZnRLZXkpO1xuICAgIH1cblxuICAgIGlmKGFsdEtleSAhPT0gZS5hbHRLZXkpXG4gICAge1xuICAgICAgICBhbHRLZXkgPSBlLmFsdEtleTtcbiAgICAgICAgTVZfU3RhdGVLZXlEb3duKFwiYWx0XCIsIGFsdEtleSk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiYWx0S2V5IFwiICsgYWx0S2V5KTtcbiAgICB9XG5cbiAgICBpZihjdHJsS2V5ICE9PSBlLmN0cmxLZXkpXG4gICAge1xuICAgICAgICBjdHJsS2V5ID0gZS5jdHJsS2V5O1xuICAgICAgICBNVl9TdGF0ZUtleURvd24oXCJjdHJsXCIsIGN0cmxLZXkpO1xuICAgICAgICBjb25zb2xlLmxvZyhcImN0cmxLZXkgXCIgKyBjdHJsS2V5KTtcbiAgICB9XG59O1xuLypcbnZhciBfbG9zdCA9IGZ1bmN0aW9uKClcbntcbiAgICBpZih0eXBlb2YgTVZfU3RhdGVLZXlEb3duID09PSBcInVuZGVmaW5lZFwiKVxuICAgICAgICByZXR1cm47XG5cbiAgICBfdXBkYXRlS2V5cyh7XG4gICAgICAgIHNoaWZ0S2V5OiBmYWxzZSxcbiAgICAgICAgYWx0S2V5OiBmYWxzZSxcbiAgICAgICAgY3RybEtleTogZmFsc2VcbiAgICB9KTtcbn07XG4qL1xud2luZG93Lm9ua2V5ZG93biA9IE1WX09uVXBkYXRlS2V5cztcbndpbmRvdy5vbmtleXVwID0gTVZfT25VcGRhdGVLZXlzO1xuLy93aW5kb3cub25jbGljayA9IF91cGRhdGVLZXlzO1xuLy93aW5kb3cub25mb2N1cyA9IF9sb3N0O1xuLy93aW5kb3cub25ibHVyID0gX2xvc3Q7XG5cbnZhciBNb3VzZSA9XG57XG4gICAgY2xpZW50WDogMCxcbiAgICBjbGllbnRZOiAwLFxuICAgIHBhZ2VYOiAwLFxuICAgIHBhZ2VZOiAwLFxufTtcblxuXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICBfbW91c2VNb3ZlKGV2ZW50LCBmYWxzZSk7XG59LCB0cnVlKTtcblxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2RyYWcnLCBmdW5jdGlvbihldmVudCkge1xuICAgIGV2ZW50LmRhdGFUcmFuc2Zlci5lZmZlY3RBbGxvd2VkID0gJ2FsbCc7XG4gICAgTVZfT25VcGRhdGVLZXlzKGV2ZW50KTtcbiAgICBfbW91c2VNb3ZlKGV2ZW50LCB0cnVlKTtcbiAgICBDb250ZW50TGF5b3V0UGFuZWwuRmluZERyb3BQYW5lbCgpO1xufSwgdHJ1ZSk7XG5cblxubGV0IF9oaWRlRHJhZ2dpbmdDdXJ0YWluID0gZmFsc2U7XG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCBmdW5jdGlvbihldmVudCkge1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiYm9keVwiKVswXS5jbGFzc0xpc3QuYWRkKFwiaS1oaWRlLWRyYWdnaW5nQ3VydGFpblwiKTsvL9CX0LDQv9C70LDRgtC60LAg0L/RgNC+0YLQuNCyINCx0LDQs9CwLCDQutC+0LPQtNCwINC30LDQstC10YHQsCDQv9C+0Y/QstC70Y/QtdGC0YHRjywg0LHQtdC3INC00LLQuNC20LXQvdC40Y8g0LrRg9GA0YHQvtGA0LBcbiAgICBfaGlkZURyYWdnaW5nQ3VydGFpbiA9IHRydWU7XG59LCB0cnVlKTtcblxuXG5mdW5jdGlvbiBfbW91c2VNb3ZlKGV2ZW50LCBpc0RyYWcpXG57XG4gICAgaWYoIWlzRHJhZyAmJiBldmVudC5tb3ZlbWVudFggPT0gMCAmJiBldmVudC5tb3ZlbWVudFkgPT0gMClcbiAgICAgICAgcmV0dXJuO1xuXG4gICAgdmFyIGV2ZW50RG9jLCBkb2MsIGJvZHk7XG5cbiAgICBldmVudCA9IGV2ZW50IHx8IHdpbmRvdy5ldmVudDtcblxuICAgIGlmKF9oaWRlRHJhZ2dpbmdDdXJ0YWluKVxuICAgIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJib2R5XCIpWzBdLmNsYXNzTGlzdC5yZW1vdmUoXCJpLWhpZGUtZHJhZ2dpbmdDdXJ0YWluXCIpO1xuICAgICAgICBfaGlkZURyYWdnaW5nQ3VydGFpbiA9IGZhbHNlO1xuICAgIH1cblxuICAgIE1vdXNlLnBhZ2VYID0gMDtcbiAgICBNb3VzZS5wYWdlWSA9IDA7XG5cbiAgICBpZiAoZXZlbnQucGFnZVggPT0gbnVsbCAmJiBldmVudC5jbGllbnRYICE9IG51bGwpIHtcbiAgICAgICAgZXZlbnREb2MgPSAoZXZlbnQudGFyZ2V0ICYmIGV2ZW50LnRhcmdldC5vd25lckRvY3VtZW50KSB8fCBkb2N1bWVudDtcbiAgICAgICAgZG9jID0gZXZlbnREb2MuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICBib2R5ID0gZXZlbnREb2MuYm9keTtcblxuICAgICAgICBNb3VzZS5wYWdlWCA9IGV2ZW50LmNsaWVudFggK1xuICAgICAgICAgICAgKGRvYyAmJiBkb2Muc2Nyb2xsTGVmdCB8fCBib2R5ICYmIGJvZHkuc2Nyb2xsTGVmdCB8fCAwKSAtXG4gICAgICAgICAgICAoZG9jICYmIGRvYy5jbGllbnRMZWZ0IHx8IGJvZHkgJiYgYm9keS5jbGllbnRMZWZ0IHx8IDApO1xuICAgICAgICBNb3VzZS5wYWdlWSA9IGV2ZW50LmNsaWVudFkgK1xuICAgICAgICAgICAgKGRvYyAmJiBkb2Muc2Nyb2xsVG9wICB8fCBib2R5ICYmIGJvZHkuc2Nyb2xsVG9wICB8fCAwKSAtXG4gICAgICAgICAgICAoZG9jICYmIGRvYy5jbGllbnRUb3AgIHx8IGJvZHkgJiYgYm9keS5jbGllbnRUb3AgIHx8IDAgKTtcbiAgICB9XG4gICAgZWxzZVxuICAgIHtcbiAgICAgICAgTW91c2UucGFnZVggPSBldmVudC5wYWdlWDtcbiAgICAgICAgTW91c2UucGFnZVkgPSBldmVudC5wYWdlWTtcbiAgICB9XG5cbiAgICBNb3VzZS5jbGllbnRYID0gZXZlbnQuY2xpZW50WDtcbiAgICBNb3VzZS5jbGllbnRZID0gZXZlbnQuY2xpZW50WTtcblxuICAgIC8vTVZfTW91c2VTdGF0ZShNb3VzZS5wYWdlWCwgTW91c2UucGFnZVksIE1vdXNlLmNsaWVudFgsIE1vdXNlLmNsaWVudFkpO1xufVxuXG4vL2Z1bmN0aW9uIE1WX01vdXNlU3RhdGUoKSB7fVxuXG5cblxuXG5mdW5jdGlvbiBNVl9TZXRTY3JvbGxQb3NpdGlvbihpZCwgc2Nyb2xsTGVmdCwgc2Nyb2xsVG9wKVxue1xuICAgIHZhciBzY3JvbGxIID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNcIiArIGlkICsgXCIgLnYtdHJlZWdyaWQtc2Nyb2xsZXItaG9yaXpvbnRhbFwiKTtcbiAgICB2YXIgc2Nyb2xsViA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjXCIgKyBpZCArIFwiIC52LXRyZWVncmlkLXNjcm9sbGVyLXZlcnRpY2FsXCIpO1xuXG4gICAgc2Nyb2xsSC5zY3JvbGxMZWZ0ID0gc2Nyb2xsTGVmdDtcbiAgICBzY3JvbGxWLnNjcm9sbFRvcCA9IHNjcm9sbFRvcDtcbn1cbmZ1bmN0aW9uIE1WX1JlcXVlc3RTY3JvbGxQb3NpdGlvbihpZClcbntcbiAgICB2YXIgc2Nyb2xsSCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjXCIgKyBpZCArIFwiIC52LXRyZWVncmlkLXNjcm9sbGVyLWhvcml6b250YWxcIik7XG4gICAgdmFyIHNjcm9sbFYgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI1wiICsgaWQgKyBcIiAudi10cmVlZ3JpZC1zY3JvbGxlci12ZXJ0aWNhbFwiKTtcblxuICAgIGlmKHNjcm9sbEgub25zY3JvbGwgIT0gbnVsbCB8fCBzY3JvbGxWLm9uc2Nyb2xsICE9IG51bGwpXG4gICAgICAgIHJldHVybjtcblxuICAgIHZhciBfZmlyZVVwZGF0ZVNjcm9sbFBvcyA9IGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgIF91cGRhdGVTY3JvbGxQb3MoaWQsIHNjcm9sbEgsIHNjcm9sbFYpO1xuICAgIH07XG5cbiAgICBzY3JvbGxILm9uc2Nyb2xsID0gX2ZpcmVVcGRhdGVTY3JvbGxQb3M7XG4gICAgc2Nyb2xsVi5vbnNjcm9sbCA9IF9maXJlVXBkYXRlU2Nyb2xsUG9zO1xuXG4gICAgX3VwZGF0ZVNjcm9sbFBvcyhpZCwgc2Nyb2xsSCwgc2Nyb2xsVik7XG59XG5cbmZ1bmN0aW9uIE1WX1N0b3BSZXF1ZXN0U2Nyb2xsUG9zaXRpb24oaWQpXG57XG4gICAgdmFyIHNjcm9sbEggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI1wiICsgaWQgKyBcIiAudi10cmVlZ3JpZC1zY3JvbGxlci1ob3Jpem9udGFsXCIpO1xuICAgIHZhciBzY3JvbGxWID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNcIiArIGlkICsgXCIgLnYtdHJlZWdyaWQtc2Nyb2xsZXItdmVydGljYWxcIik7XG5cbiAgICBzY3JvbGxILm9uc2Nyb2xsID0gbnVsbDtcbiAgICBzY3JvbGxWLm9uc2Nyb2xsID0gbnVsbDtcbn1cblxuXG5mdW5jdGlvbiBfdXBkYXRlU2Nyb2xsUG9zKGlkLCBzY3JvbGxILCBzY3JvbGxWKVxue1xuICAgIE1WX1JlcXVlc3RTY3JvbGxQb3NpdGlvbkxpc3RlbmVyKFxuICAgICAgICBpZCxcbiAgICAgICAge1xuICAgICAgICAgICAgc2Nyb2xsTGVmdDogICAgIHNjcm9sbEguc2Nyb2xsTGVmdCxcbiAgICAgICAgICAgIHNjcm9sbFRvcDogICAgICBzY3JvbGxWLnNjcm9sbFRvcFxuICAgICAgICB9KTtcbn1cblxuXG5cblxuZnVuY3Rpb24gTVZfUmVxdWVzdEJvdW5kaW5nQ2xpZW50UmVjdChpZCwgaXNSZWxhdGl2ZSlcbntcbiAgICB2YXIgb2JqID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpO1xuICAgIHZhciByZWN0ID0gb2JqLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuXG4gICAgdmFyIHNjcm9sbFggPSBpc1JlbGF0aXZlID8gd2luZG93LnNjcm9sbFggOiAwO1xuICAgIHZhciBzY3JvbGxZID0gaXNSZWxhdGl2ZSA/IHdpbmRvdy5zY3JvbGxZIDogMDtcblxuICAgIE1WX1JlcXVlc3RCb3VuZGluZ0xpc3RlbmVyKFxuICAgICAgICBpZCxcbiAgICAgICAge1xuICAgICAgICAgICAgbV9MZWZ0OiAgICAgcmVjdC5sZWZ0ICAgKyBzY3JvbGxYLFxuICAgICAgICAgICAgbV9SaWdodDogICAgcmVjdC5yaWdodCAgKyBzY3JvbGxYLFxuICAgICAgICAgICAgbV9Cb3R0b206ICAgcmVjdC5ib3R0b20gKyBzY3JvbGxZLFxuICAgICAgICAgICAgbV9Ub3A6ICAgICAgcmVjdC50b3AgICAgKyBzY3JvbGxZLFxuXG4gICAgICAgICAgICBtX0hlaWdodDogICByZWN0LmhlaWdodCxcbiAgICAgICAgICAgIG1fV2lkdGg6ICAgIHJlY3Qud2lkdGgsXG5cbiAgICAgICAgICAgIG1fWDogICAgICAgIHJlY3QueCArIHNjcm9sbFgsXG4gICAgICAgICAgICBtX1k6ICAgICAgICByZWN0LnkgKyBzY3JvbGxZXG4gICAgICAgIH0pO1xufVxuXG5cblxudmFyIENvbnRlbnRMYXlvdXRQYW5lbCA9IHtcbiAgICBjb250ZW50TGF5b3V0UGFuZWw6IG51bGwsXG4gICAgbWFpblBhbmVsOiBudWxsLFxuICAgIHNwbGl0UGFuZWxzOiBudWxsLFxuICAgIGZvY3VzUGFuZWw6IG51bGwsXG4gICAgdGFyZ2V0UG9zaXRpb246IG51bGwsXG4gICAgaXNEcmFnZ2VkOiBmYWxzZSxcbiAgICBpc0luaXQ6IC0xLFxuICAgIGRyYWdnZWRTcGxpdHRlcjogbnVsbCxcbiAgICBtYWluU3R5bGVTaGVldDogbnVsbCxcbiAgICBVcGRhdGVDbGllbnQ6IGZ1bmN0aW9uIChtYWluUGFuZWwpXG4gICAge1xuXG4gICAgICAgIGlmKHRoaXMuaXNJbml0ID09PSAtMSlcbiAgICAgICAgICAgIHRoaXMuaXNJbml0ID0gMDtcblxuICAgICAgICBtYWluUGFuZWwucGFyZW50ID0gbnVsbDtcbiAgICAgICAgbWFpblBhbmVsLmluZGV4ID0gLTE7XG5cbiAgICAgICAgdGhpcy5tYWluUGFuZWwgPSBtYWluUGFuZWw7XG4gICAgICAgIHRoaXMuc3BsaXRQYW5lbHMgPSB7fTtcbiAgICAgICAgdGhpcy5mb2N1c1BhbmVsID0gbnVsbDtcblxuICAgICAgICBpZih0aGlzLmlzSW5pdCA9PT0gMClcbiAgICAgICAge1xuICAgICAgICAgICAgc2V0VGltZW91dCgnQ29udGVudExheW91dFBhbmVsLl9pbml0KCknLDApO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgc2V0VGltZW91dCgnQ29udGVudExheW91dFBhbmVsLl9kb1BhcmNlVXBkYXRlKCknLDApO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBfZG9QYXJjZVVwZGF0ZTpmdW5jdGlvbigpXG4gICAge1xuICAgICAgICB0aGlzLl9wYXJzZVBhbmVsKHRoaXMubWFpblBhbmVsKTtcbiAgICAgICAgdGhpcy5VcGRhdGUodGhpcy5tYWluUGFuZWwpO1xuICAgIH0sXG4gICAgX2luaXQ6ZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAgdGhpcy5tYWluU3R5bGVTaGVldCA9ICBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcInN0eWxlXCIpWzBdLnNoZWV0O1xuXG4gICAgICAgIHRoaXMuY29udGVudExheW91dFBhbmVsID0gZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb250ZW50TGF5b3V0UGFuZWxcIik7XG4gICAgICAgIH07XG5cblxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBfZGVib3VuY2UoZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICBDb250ZW50TGF5b3V0UGFuZWwuVXBkYXRlKENvbnRlbnRMYXlvdXRQYW5lbC5tYWluUGFuZWwpO1xuICAgICAgICAgICAgLy9fdXBkYXRlKCk7XG4gICAgICAgIH0pKTtcblxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNldXBcIiwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIGlmKENvbnRlbnRMYXlvdXRQYW5lbC5kcmFnZ2VkU3BsaXR0ZXIgIT0gbnVsbClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihDb250ZW50TGF5b3V0UGFuZWwuZHJhZ2dlZFNwbGl0dGVyLnBhcmVudFBhbmVsLmlzRGlydHkpXG4gICAgICAgICAgICAgICAgICAgIENvbnRlbnRMYXlvdXRQYW5lbC5VcGRhdGUoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlci5wYXJlbnRQYW5lbCk7XG5cbiAgICAgICAgICAgICAgICBDb250ZW50TGF5b3V0UGFuZWwuY29udGVudExheW91dFBhbmVsKCkuc2V0QXR0cmlidXRlKFwic3RhdGVcIiwgXCJcIik7XG4gICAgICAgICAgICAgICAgQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlciA9IG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSx0cnVlKTtcblxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBmdW5jdGlvbihldmVudClcbiAgICAgICAge1xuICAgICAgICAgICAgaWYoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlciAhPSBudWxsKVxuICAgICAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICAgICAgdmFyIF9zcGxpdHRlciA9IGV2ZW50LnRhcmdldDtcbiAgICAgICAgICAgIGlmKF9zcGxpdHRlci5jbGFzc0xpc3QuY29udGFpbnMoXCJ2LXNwbGl0cGFuZWwtaHNwbGl0dGVyXCIpIHx8IF9zcGxpdHRlci5jbGFzc0xpc3QuY29udGFpbnMoXCJ2LXNwbGl0cGFuZWwtdnNwbGl0dGVyXCIpKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhciBfc3BsaXRQYW5lbCA9IF9zcGxpdHRlci5wYXJlbnRFbGVtZW50LnBhcmVudEVsZW1lbnQ7XG5cbiAgICAgICAgICAgICAgICBDb250ZW50TGF5b3V0UGFuZWwuZHJhZ2dlZFNwbGl0dGVyID0gQ29udGVudExheW91dFBhbmVsLnNwbGl0UGFuZWxzW19zcGxpdFBhbmVsLmlkXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBDb250ZW50TGF5b3V0UGFuZWwuZHJhZ2dlZFNwbGl0dGVyID0gbnVsbDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlciAhPSBudWxsKVxuICAgICAgICAgICAgICAgIENvbnRlbnRMYXlvdXRQYW5lbC5jb250ZW50TGF5b3V0UGFuZWwoKS5zZXRBdHRyaWJ1dGUoXCJzdGF0ZVwiLCBcImFjdGl2ZVwiKTtcblxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIm1vdXNlZG93blwiKVxuICAgICAgICB9LCB0cnVlKTtcblxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCBmdW5jdGlvbihldmVudClcbiAgICAgICAge1xuICAgICAgICAgICAgaWYoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlciA9PSBudWxsKVxuICAgICAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICAgICAgaWYoQ29udGVudExheW91dFBhbmVsLl9tb3ZlU3BsaXR0ZXIoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlciwgbnVsbCkpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlci5wYXJlbnRQYW5lbC5pc0RpcnR5ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAvL0NvbnRlbnRMYXlvdXRQYW5lbC5VcGRhdGUoQ29udGVudExheW91dFBhbmVsLmRyYWdnZWRTcGxpdHRlci5wYXJlbnRQYW5lbCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vQ29udGVudExheW91dFBhbmVsLlVwZGF0ZShbX3MuZmlyc3RQYW5lbCwgX3Muc2Vjb25kUGFuZWxdKTtcblxuICAgICAgICB9LCB0cnVlKTtcblxuICAgICAgICB0aGlzLmlzSW5pdCA9IDE7XG5cbiAgICAgICAgQ29udGVudExheW91dFBhbmVsLl9kb1BhcmNlVXBkYXRlKCk7XG4gICAgfSxcbiAgICBfdXBkYXRlU3BsaXRQYW5lbHM6IGZ1bmN0aW9uKHNwbGl0UGFuZWwsIHN0YXJ0UGFuZWxJbmRleCwgZW5kUGFuZWxJbmRleCwgY3VySW5kZXgsIGRpcmVjdGlvbilcbiAgICB7XG4gICAgICAgIGlmKHN0YXJ0UGFuZWxJbmRleCA+IGVuZFBhbmVsSW5kZXgpXG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgdmFyIF9zID0gc3BsaXRQYW5lbC5zcGxpdHNbc3RhcnRQYW5lbEluZGV4XTtcbiAgICAgICAgdGhpcy5fdXBkYXRlU3BsaXRQYW5lbHMoX3MuZmlyc3RQYW5lbCwgMCwgX3MuZmlyc3RQYW5lbC5zcGxpdHMubGVuZ3RoIC0gMSwgLTEsIGRpcmVjdGlvbik7XG5cbiAgICAgICAgZm9yKHZhciBpID0gc3RhcnRQYW5lbEluZGV4OyBpIDw9IGVuZFBhbmVsSW5kZXg7IGkrKylcbiAgICAgICAge1xuICAgICAgICAgICAgX3MgPSBzcGxpdFBhbmVsLnNwbGl0c1tpXTtcblxuXG4gICAgICAgICAgICBpZihzcGxpdFBhbmVsLmRpcmVjdGlvbiA9PT0gZGlyZWN0aW9uICYmIGkgIT0gY3VySW5kZXgpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIF9zcGxpdFBhbmVsV3JhcHBlciA9IF9zLkdldFNwbGl0UGFuZWxXcmFwcGVyKCk7XG4gICAgICAgICAgICAgICAgdmFyIF9zcGxpdHRlciA9IF9zLkdldFNwbGl0dGVyRWxlbWVudCgpO1xuXG4gICAgICAgICAgICAgICAgaWYoc3BsaXRQYW5lbC5kaXJlY3Rpb24gPT09IFwiSG9yaXpvbnRhbFwiKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgX3NwbGl0dGVyLnN0eWxlLmxlZnQgPSAgTWF0aC5yb3VuZChfc3BsaXRQYW5lbFdyYXBwZXIub2Zmc2V0V2lkdGggKiBfcy5wb3NpdGlvbikgKyBcInB4XCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIF9zcGxpdHRlci5zdHlsZS50b3AgPSAgTWF0aC5yb3VuZChfc3BsaXRQYW5lbFdyYXBwZXIub2Zmc2V0SGVpZ2h0ICogX3MucG9zaXRpb24pICsgXCJweFwiO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5fdXBkYXRlU3BsaXRQYW5lbHMoX3Muc2Vjb25kUGFuZWwsIDAsIF9zLnNlY29uZFBhbmVsLnNwbGl0cy5sZW5ndGggLSAxLCAtMSwgZGlyZWN0aW9uKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgX21vdmVTcGxpdHRlcjogZnVuY3Rpb24oX3NwbGl0LCBwb3NpdGlvbilcbiAgICB7XG5cbiAgICAgICAgdmFyIF9zcGxpdHRlciA9IF9zcGxpdC5HZXRTcGxpdHRlckVsZW1lbnQoKTtcbiAgICAgICAgdmFyIF9zcGxpdFBhbmVsV3JhcHBlciA9IF9zcGxpdC5HZXRTcGxpdFBhbmVsV3JhcHBlcigpO1xuXG4gICAgICAgIHZhciBfZmlyc3RFbGVtZW50ID0gX3NwbGl0LmZpcnN0UGFuZWwuR2V0RWxlbWVudCgpLnBhcmVudEVsZW1lbnQ7XG4gICAgICAgIHZhciBfc2Vjb25kRWxlbWVudCA9IF9zcGxpdC5zZWNvbmRQYW5lbC5HZXRFbGVtZW50KCkucGFyZW50RWxlbWVudDtcblxuICAgICAgICAvL3ZhciBfcGFyZW50ID0gX3NwbGl0LnBhcmVudFBhbmVsLkdldEVsZW1lbnQoKTtcblxuICAgICAgICBpZihwb3NpdGlvbiAhPSBudWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICBfc3BsaXQucG9zaXRpb24gPSBNYXRoLnJvdW5kKHBvc2l0aW9uICogMTAwMCkgLyAxMDAwO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYoX3NwbGl0LnBhcmVudFBhbmVsLmRpcmVjdGlvbiA9PT0gXCJIb3Jpem9udGFsXCIpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmKHBvc2l0aW9uID09IG51bGwpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIF93aWR0aCA9IE1hdGgubWF4KDEsIF9zcGxpdFBhbmVsV3JhcHBlci5vZmZzZXRXaWR0aCk7XG5cbiAgICAgICAgICAgICAgICB2YXIgbmV3UG9zaXRpb24gPSBNYXRoLnJvdW5kKChfc3BsaXR0ZXIub2Zmc2V0TGVmdCArIF9zcGxpdHRlci5vZmZzZXRXaWR0aCAqIDAuNSkgLyBfd2lkdGggKiAxMDAwKSAvIDEwMDA7XG4gICAgICAgICAgICAgICAgaWYobmV3UG9zaXRpb24gPT09IF9zcGxpdC5wb3NpdGlvbilcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgX3NwbGl0LnBvc2l0aW9uID0gbmV3UG9zaXRpb25cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIF9wb3MgPSBcIigxMDAlIC0gKFwiICsgX3NwbGl0UGFuZWxXcmFwcGVyLnN0eWxlLmxlZnQgKyBcIiArIFwiICsgX3NwbGl0UGFuZWxXcmFwcGVyLnN0eWxlLnJpZ2h0ICsgXCIpKSAqIFwiICsgX3NwbGl0LnBvc2l0aW9uICsgXCIgKyBcIiArIF9zcGxpdFBhbmVsV3JhcHBlci5zdHlsZS5sZWZ0O1xuXG4gICAgICAgICAgICBpZihfc3BsaXQuaW5kZXggPT09IDApXG4gICAgICAgICAgICAgICAgX2ZpcnN0RWxlbWVudC5zdHlsZS5sZWZ0ID0gXCIwcHhcIjtcbiAgICAgICAgICAgIGlmKF9zcGxpdC5pbmRleCA9PT0gX3NwbGl0LnBhcmVudFBhbmVsLnNwbGl0cy5sZW5ndGggLSAxKVxuICAgICAgICAgICAgICAgIF9zZWNvbmRFbGVtZW50LnN0eWxlLnJpZ2h0ID0gXCIwcHhcIjtcblxuICAgICAgICAgICAgX2ZpcnN0RWxlbWVudC5zdHlsZS5yaWdodCA9IFwiY2FsYygxMDAlIC0gKFwiICsgX3BvcyArIFwiKSlcIjtcbiAgICAgICAgICAgIF9zZWNvbmRFbGVtZW50LnN0eWxlLmxlZnQgPSBcImNhbGMoXCIgKyBfcG9zICsgXCIpXCI7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZVxuICAgICAgICB7XG4gICAgICAgICAgICBpZihwb3NpdGlvbiA9PSBudWxsKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhciBfaGVpZ2h0ID0gTWF0aC5tYXgoMSwgX3NwbGl0UGFuZWxXcmFwcGVyLm9mZnNldEhlaWdodCk7XG4gICAgICAgICAgICAgICAgdmFyIG5ld1Bvc2l0aW9uID0gTWF0aC5yb3VuZCgoX3NwbGl0dGVyLm9mZnNldFRvcCArIF9zcGxpdHRlci5vZmZzZXRIZWlnaHQgKiAwLjUpIC8gX2hlaWdodCAqIDEwMDApIC8gMTAwMDtcbiAgICAgICAgICAgICAgICBpZihuZXdQb3NpdGlvbiA9PT0gX3NwbGl0LnBvc2l0aW9uKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICBfc3BsaXQucG9zaXRpb24gPSBuZXdQb3NpdGlvblxuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIHZhciBfcG9zID0gXCIoMTAwJSAtIChcIiArIF9zcGxpdFBhbmVsV3JhcHBlci5zdHlsZS50b3AgKyBcIiArIFwiICsgX3NwbGl0UGFuZWxXcmFwcGVyLnN0eWxlLmJvdHRvbSArIFwiKSkgKiBcIiArIF9zcGxpdC5wb3NpdGlvbiArIFwiICsgXCIgKyBfc3BsaXRQYW5lbFdyYXBwZXIuc3R5bGUudG9wIDtcblxuICAgICAgICAgICAgaWYoX3NwbGl0LmluZGV4ID09PSAwKVxuICAgICAgICAgICAgICAgIF9maXJzdEVsZW1lbnQuc3R5bGUudG9wID0gXCIwcHhcIjtcbiAgICAgICAgICAgIGlmKF9zcGxpdC5pbmRleCA9PT0gX3NwbGl0LnBhcmVudFBhbmVsLnNwbGl0cy5sZW5ndGggLSAxKVxuICAgICAgICAgICAgICAgIF9zZWNvbmRFbGVtZW50LnN0eWxlLmJvdHRvbSA9IFwiMHB4XCI7XG5cbiAgICAgICAgICAgIF9maXJzdEVsZW1lbnQuc3R5bGUuYm90dG9tID0gXCJjYWxjKDEwMCUgLSAoXCIgKyBfcG9zICsgXCIpKVwiO1xuICAgICAgICAgICAgX3NlY29uZEVsZW1lbnQuc3R5bGUudG9wID0gXCJjYWxjKFwiICsgX3BvcyArIFwiKVwiO1xuICAgICAgICB9XG5cblxuICAgICAgICBpZihwb3NpdGlvbiA9PSBudWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgc3RhcnRQYW5lbEluZGV4ID0gX3NwbGl0LmluZGV4O1xuICAgICAgICAgICAgdmFyIGVuZFBhbmVsSW5kZXggPSBfc3BsaXQuaW5kZXg7XG5cbiAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBfc3BsaXQucGFyZW50UGFuZWwuc3BsaXRzLmxlbmd0aDsgaSsrKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhciBfcyA9IF9zcGxpdC5wYXJlbnRQYW5lbC5zcGxpdHNbaV07XG5cbiAgICAgICAgICAgICAgICBpZihpIDwgX3NwbGl0LmluZGV4KVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoX3MucG9zaXRpb24gPiBfc3BsaXQucG9zaXRpb24pXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX21vdmVTcGxpdHRlcihfcywgX3NwbGl0LnBvc2l0aW9uKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoaSA8IHN0YXJ0UGFuZWxJbmRleClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydFBhbmVsSW5kZXggPSBpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYoaSA+IF9zcGxpdC5pbmRleClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlmKF9zLnBvc2l0aW9uIDwgX3NwbGl0LnBvc2l0aW9uKVxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9tb3ZlU3BsaXR0ZXIoX3MsIF9zcGxpdC5wb3NpdGlvbik7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGkgPiBlbmRQYW5lbEluZGV4KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVuZFBhbmVsSW5kZXggPSBpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVTcGxpdFBhbmVscyhfc3BsaXQucGFyZW50UGFuZWwsIHN0YXJ0UGFuZWxJbmRleCwgZW5kUGFuZWxJbmRleCwgX3NwbGl0LmluZGV4LCBfc3BsaXQucGFyZW50UGFuZWwuZGlyZWN0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH0sXG4gICAgX3BhcnNlUGFuZWw6IGZ1bmN0aW9uKGN1clBhbmVsKVxuICAgIHtcbiAgICAgICAgLypcbiAgICAgICAgY3VyUGFuZWwuR2V0RWxlbWVudCA9IGZ1bmN0aW9uKHVwZGF0ZUNhY2hlKVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fY2FjaGVFbGVtZW50ID09IG51bGwgPyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0aGlzLmlkKSA6IHRoaXMuX2NhY2hlRWxlbWVudDtcbiAgICAgICAgfTtcblxuICAgICAgICBjdXJQYW5lbC5HZXRGb3JtRWxlbWVudCA9IGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZm9ybSA9PSBudWxsID8gbnVsbCA6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuZm9ybSk7XG4gICAgICAgIH07Ki9cblxuICAgICAgICBjdXJQYW5lbC5HZXRFbGVtZW50ID0gZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5pZCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgY3VyUGFuZWwuR2V0Rm9ybUVsZW1lbnQgPSBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0aGlzLmZvcm0pO1xuICAgICAgICB9O1xuXG5cbiAgICAgICAgdmFyIHBhbmVsU3R5bGUgPSB0aGlzLl9nZXRTdHlsZVJ1bGUoXCIuaS1Ecm9wUGFuZWxcIiwgY3VyUGFuZWwuaWQpO1xuXG4gICAgICAgIHBhbmVsU3R5bGUuc2V0UHJvcGVydHkoXCJtaW4td2lkdGhcIiwgY3VyUGFuZWwubWluV2lkdGggKyBcInB4XCIsICdpbXBvcnRhbnQnKTtcbiAgICAgICAgcGFuZWxTdHlsZS5zZXRQcm9wZXJ0eShcIm1pbi1oZWlnaHRcIiwgY3VyUGFuZWwubWluSGVpZ2h0ICsgXCJweFwiLCAnaW1wb3J0YW50Jyk7XG5cblxuICAgICAgICAvL3NoZWV0LiQkKFwiY2VsbHNcIikuZ2V0SXRlbSgzKVsyXVxuXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBjdXJQYW5lbC5zcGxpdHMubGVuZ3RoOyBpKyspXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBfcyA9IGN1clBhbmVsLnNwbGl0c1tpXTtcblxuICAgICAgICAgICAgX3MuZmlyc3RQYW5lbCA9IGN1clBhbmVsLmNoaWxkc1tpXTtcbiAgICAgICAgICAgIF9zLnNlY29uZFBhbmVsID0gY3VyUGFuZWwuY2hpbGRzW2kgKyAxXTtcbiAgICAgICAgICAgIF9zLnBhcmVudFBhbmVsID0gY3VyUGFuZWw7XG4gICAgICAgICAgICBfcy5pbmRleCA9IGk7XG5cbiAgICAgICAgICAgIF9zLkdldFNwbGl0dGVyRWxlbWVudCA9IGZ1bmN0aW9uKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5pZCkuY2hpbGROb2Rlc1swXS5jaGlsZE5vZGVzWzFdO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgX3MuR2V0U3BsaXRQYW5lbFdyYXBwZXIgPSBmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuaWQpLnBhcmVudEVsZW1lbnQ7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICB0aGlzLnNwbGl0UGFuZWxzW19zLmlkXSA9IF9zO1xuICAgICAgICB9XG5cbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGN1clBhbmVsLmNoaWxkcy5sZW5ndGg7IGkrKylcbiAgICAgICAge1xuICAgICAgICAgICAgY3VyUGFuZWwuY2hpbGRzW2ldLnBhcmVudCA9IGN1clBhbmVsO1xuICAgICAgICAgICAgY3VyUGFuZWwuY2hpbGRzW2ldLmluZGV4ID0gaTtcblxuICAgICAgICAgICAgdGhpcy5fcGFyc2VQYW5lbChjdXJQYW5lbC5jaGlsZHNbaV0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGN1clBhbmVsLnNwbGl0cy5sZW5ndGg7IGkrKylcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIF9zID0gY3VyUGFuZWwuc3BsaXRzW2ldO1xuXG4gICAgICAgICAgICAvL19zLmZpcnN0UGFuZWwuR2V0RWxlbWVudCh0cnVlKTtcbiAgICAgICAgICAgIC8vX3Muc2Vjb25kUGFuZWwuR2V0RWxlbWVudCh0cnVlKTtcblxuICAgICAgICAgICAgQ29udGVudExheW91dFBhbmVsLl9tb3ZlU3BsaXR0ZXIoX3MsIF9zLnBvc2l0aW9uKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgVXBkYXRlOiBmdW5jdGlvbihwYW5lbClcbiAgICB7XG4gICAgICAgIGlmKHRoaXMuaXNJbml0ICE9PSAxKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJVcGRhdGVcIilcblxuICAgICAgICB2YXIgcGFuZWxUcmFuc2Zvcm1zID0ge307XG5cbiAgICAgICAgQ29udGVudExheW91dFBhbmVsLl91cGRhdGVQYW5lbChwYW5lbCwgcGFuZWxUcmFuc2Zvcm1zKTtcbiAgICAgICAgTVZfQ29udGVudExheW91dFBhbmVsX1VwZGF0ZVNlcnZlcihwYW5lbFRyYW5zZm9ybXMpO1xuXG5cbiAgICAgICAgcGFuZWwuaXNEaXJ0eSA9IGZhbHNlO1xuICAgIH0sXG4gICAgX3VwZGF0ZVBhbmVsOiBmdW5jdGlvbihjdXJQYW5lbCwgcGFuZWxUcmFuc2Zvcm1zKVxuICAgIHtcbiAgICAgICAgaWYoY3VyUGFuZWwgPT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICB2YXIgcmVjdCA9IGN1clBhbmVsLkdldEVsZW1lbnQoKTtcbiAgICAgICAgdmFyIGZvcm0gPSBjdXJQYW5lbC5HZXRGb3JtRWxlbWVudCgpO1xuXG4gICAgICAgIGlmKHJlY3QgPT0gbnVsbClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGdnID0gXCJcIjtcbiAgICAgICAgICAgIGdnICs9IFwid2VcIjtcblxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGJSZWN0ID0gcmVjdC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuXG4gICAgICAgIHZhciBfbGVmdCA9IE1hdGgucm91bmQoYlJlY3QubGVmdCk7XG4gICAgICAgIHZhciBfdG9wID0gTWF0aC5yb3VuZChiUmVjdC50b3ApO1xuICAgICAgICB2YXIgX3dpZHRoID0gTWF0aC5yb3VuZChiUmVjdC53aWR0aCk7XG4gICAgICAgIHZhciBfaGVpZ2h0ID0gTWF0aC5yb3VuZChiUmVjdC5oZWlnaHQpO1xuXG4gICAgICAgIHZhciBfc3BsaXRzUG9zaXRpb25zID0gW107XG5cbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGN1clBhbmVsLnNwbGl0cy5sZW5ndGg7IGkrKylcbiAgICAgICAge1xuICAgICAgICAgICAgX3NwbGl0c1Bvc2l0aW9ucy5wdXNoKGN1clBhbmVsLnNwbGl0c1tpXS5wb3NpdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICBwYW5lbFRyYW5zZm9ybXNbY3VyUGFuZWwuaWRdID0ge1xuICAgICAgICAgICAgbGVmdDogX2xlZnQsXG4gICAgICAgICAgICB0b3A6IF90b3AsXG4gICAgICAgICAgICB3aWR0aDogX3dpZHRoLFxuICAgICAgICAgICAgaGVpZ2h0OiBfaGVpZ2h0LFxuICAgICAgICAgICAgc3BsaXRzUG9zaXRpb25zOiBfc3BsaXRzUG9zaXRpb25zXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYoZm9ybSAhPSBudWxsLyogJiYgZm9ybS5jbGFzc0xpc3QuY29udGFpbnMoXCJpLWlzLWVtYmVkZGVkXCIpKi8pXG4gICAgICAgIHtcbiAgICAgICAgICAgIGZvcm0uc3R5bGUubGVmdCA9IF9sZWZ0ICsgXCJweFwiO1xuICAgICAgICAgICAgZm9ybS5zdHlsZS50b3AgPSBfdG9wICsgXCJweFwiO1xuICAgICAgICAgICAgZm9ybS5zdHlsZS53aWR0aCA9IF93aWR0aCArIFwicHhcIjtcbiAgICAgICAgICAgIGZvcm0uc3R5bGUuaGVpZ2h0ID0gX2hlaWdodCArIFwicHhcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBjdXJQYW5lbC5jaGlsZHMubGVuZ3RoOyBpKyspXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMuX3VwZGF0ZVBhbmVsKGN1clBhbmVsLmNoaWxkc1tpXSwgcGFuZWxUcmFuc2Zvcm1zKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgX2dldFN0eWxlUnVsZTogZnVuY3Rpb24oc2VsZWN0b3IsIGlkKVxuICAgIHtcbiAgICAgICAgdmFyIHN0eWxlUnVsZSA9IG51bGw7XG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLm1haW5TdHlsZVNoZWV0LnJ1bGVzLmxlbmd0aDsgaSsrKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZih0aGlzLm1haW5TdHlsZVNoZWV0LnJ1bGVzW2ldLnNlbGVjdG9yVGV4dC5pbmRleE9mKHNlbGVjdG9yICsgXCIjXCIgKyBpZCkgPj0gMClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdHlsZVJ1bGUgPSB0aGlzLm1haW5TdHlsZVNoZWV0LnJ1bGVzW2ldO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmKHN0eWxlUnVsZSA9PSBudWxsKVxuICAgICAgICAgICAgc3R5bGVSdWxlID0gdGhpcy5tYWluU3R5bGVTaGVldC5ydWxlc1t0aGlzLm1haW5TdHlsZVNoZWV0Lmluc2VydFJ1bGUoc2VsZWN0b3IgKyBcIiNcIiArIGlkICsgXCJ7fVwiKV07XG5cbiAgICAgICAgcmV0dXJuIHN0eWxlUnVsZS5zdHlsZTtcbiAgICB9LFxuICAgIFN0YXJ0OiBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgaWYodGhpcy5pc0luaXQgIT09IDEpXG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgdGhpcy5pc0RyYWdnZWQgPSB0cnVlO1xuICAgIH0sXG4gICAgUmVsZWFzZTogZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIGlmKHRoaXMuaXNJbml0ICE9PSAxIHx8ICF0aGlzLmlzRHJhZ2dlZClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICB0aGlzLmlzRHJhZ2dlZCA9IGZhbHNlO1xuXG4gICAgICAgIGlmKHRoaXMuZm9jdXNQYW5lbCA9PSBudWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICBNVl9PblJlbGVhc2VDb250ZW50TGF5b3V0UGFuZWwobnVsbCwgbnVsbCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBNVl9PblJlbGVhc2VDb250ZW50TGF5b3V0UGFuZWwodGhpcy5mb2N1c1BhbmVsLmlkLCB0aGlzLnRhcmdldFBvc2l0aW9uKTtcblxuICAgICAgICB0aGlzLmZvY3VzUGFuZWwuR2V0RWxlbWVudCgpLnNldEF0dHJpYnV0ZShcImFjdGl2ZVwiLCBudWxsKTtcblxuICAgICAgICB0aGlzLmZvY3VzUGFuZWwgPSBudWxsO1xuICAgIH0sXG4gICAgRmluZERyb3BQYW5lbDogZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIGlmKHRoaXMuaXNJbml0ICE9PSAxIHx8ICF0aGlzLmlzRHJhZ2dlZClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICB2YXIgZm9jdXNQYW5lbCA9IHRoaXMuX2ZpbmRGb2N1c1BhbmVsKHRoaXMubWFpblBhbmVsKTtcbiAgICAgICAgdmFyIHRhcmdldFBvc2l0aW9uID0gZm9jdXNQYW5lbCA9PSBudWxsID8gbnVsbCA6IHRoaXMuX2NhbGNUYXJnZXRQb3NpdGlvbihmb2N1c1BhbmVsLCBmb2N1c1BhbmVsLkdldEVsZW1lbnQoKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSk7XG5cbiAgICAgICAgd2hpbGUoIShmb2N1c1BhbmVsID09IG51bGwgfHxcbiAgICAgICAgICAgICAgICB0YXJnZXRQb3NpdGlvbiA9PSBudWxsIHx8XG4gICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPT09IFwiY2VudGVyXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwibGVmdFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcInJpZ2h0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwidG9wXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tXCIpKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZihmb2N1c1BhbmVsID09PSB0aGlzLm1haW5QYW5lbClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZih0YXJnZXRQb3NpdGlvbiA9PT0gXCJ0b3BfbGVmdFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcInRvcF9yaWdodFwiKVxuICAgICAgICAgICAgICAgICAgICB0YXJnZXRQb3NpdGlvbiA9IFwidG9wXCI7XG4gICAgICAgICAgICAgICAgZWxzZSBpZih0YXJnZXRQb3NpdGlvbiA9PT0gXCJsZWZ0X3RvcFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcImxlZnRfYm90dG9tXCIpXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldFBvc2l0aW9uID0gXCJsZWZ0XCI7XG4gICAgICAgICAgICAgICAgZWxzZSBpZih0YXJnZXRQb3NpdGlvbiA9PT0gXCJyaWdodF90b3BcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJyaWdodF9ib3R0b21cIilcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPSBcInJpZ2h0XCI7XG4gICAgICAgICAgICAgICAgZWxzZSBpZih0YXJnZXRQb3NpdGlvbiA9PT0gXCJib3R0b21fbGVmdFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcImJvdHRvbV9yaWdodFwiKVxuICAgICAgICAgICAgICAgICAgICB0YXJnZXRQb3NpdGlvbiA9IFwiYm90dG9tXCI7XG5cbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoZm9jdXNQYW5lbC5kaXJlY3Rpb24gPT09IFwiSG9yaXpvbnRhbFwiKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmKGZvY3VzUGFuZWwuaW5kZXggPT09IDAgJiZcbiAgICAgICAgICAgICAgICAgICAgKHRhcmdldFBvc2l0aW9uID09PSBcImxlZnRfdG9wXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwidG9wX2xlZnRcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJyaWdodF90b3BcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJ0b3BfcmlnaHRcIikpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKGZvY3VzUGFuZWwuaW5kZXggPT09IChmb2N1c1BhbmVsLnBhcmVudC5jaGlsZHMubGVuZ3RoIC0gMSkgJiZcbiAgICAgICAgICAgICAgICAgICAgKHRhcmdldFBvc2l0aW9uID09PSBcImxlZnRfYm90dG9tXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tX2xlZnRcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJyaWdodF9ib3R0b21cIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJib3R0b21fcmlnaHRcIikpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZih0YXJnZXRQb3NpdGlvbiA9PT0gXCJsZWZ0X3RvcFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcInRvcF9sZWZ0XCIgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFBvc2l0aW9uID09PSBcImxlZnRfYm90dG9tXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tX2xlZnRcIilcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPSBcImxlZnRcIjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmKHRhcmdldFBvc2l0aW9uID09PSBcInJpZ2h0X2JvdHRvbVwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcImJvdHRvbV9yaWdodFwiIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRQb3NpdGlvbiA9PT0gXCJyaWdodF90b3BcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJ0b3BfcmlnaHRcIilcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPSBcInJpZ2h0XCI7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvL2JyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoZm9jdXNQYW5lbC5kaXJlY3Rpb24gPT09IFwiVmVydGljYWxcIilcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihmb2N1c1BhbmVsLmluZGV4ID09PSAwICYmXG4gICAgICAgICAgICAgICAgICAgICh0YXJnZXRQb3NpdGlvbiA9PT0gXCJ0b3BfbGVmdFwiIHx8IHRhcmdldFBvc2l0aW9uID09PSBcImxlZnRfdG9wXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tX2xlZnRcIiB8fCB0YXJnZXRQb3NpdGlvbiA9PT0gXCJsZWZ0X2JvdHRvbVwiKSlcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYoZm9jdXNQYW5lbC5pbmRleCA9PT0gKGZvY3VzUGFuZWwucGFyZW50LmNoaWxkcy5sZW5ndGggLSAxKSAmJlxuICAgICAgICAgICAgICAgICAgICAodGFyZ2V0UG9zaXRpb24gPT09IFwidG9wX3JpZ2h0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwicmlnaHRfdG9wXCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tX3JpZ2h0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwicmlnaHRfYm90dG9tXCIpKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWYodGFyZ2V0UG9zaXRpb24gPT09IFwidG9wX3JpZ2h0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwicmlnaHRfdG9wXCIgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFBvc2l0aW9uID09PSBcInRvcF9sZWZ0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwibGVmdF90b3BcIilcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPSBcInRvcFwiO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYodGFyZ2V0UG9zaXRpb24gPT09IFwiYm90dG9tX3JpZ2h0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwicmlnaHRfYm90dG9tXCIgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFBvc2l0aW9uID09PSBcImJvdHRvbV9sZWZ0XCIgfHwgdGFyZ2V0UG9zaXRpb24gPT09IFwibGVmdF9ib3R0b21cIilcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0UG9zaXRpb24gPSBcImJvdHRvbVwiO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy9icmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZvY3VzUGFuZWwgPSBmb2N1c1BhbmVsLnBhcmVudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmKHRoaXMuZm9jdXNQYW5lbCAhPSBudWxsICYmIHRoaXMuZm9jdXNQYW5lbCAhPT0gZm9jdXNQYW5lbClcbiAgICAgICAgICAgIHRoaXMuZm9jdXNQYW5lbC5HZXRFbGVtZW50KCkuc2V0QXR0cmlidXRlKFwiYWN0aXZlXCIsIG51bGwpO1xuXG4gICAgICAgIHRoaXMuZm9jdXNQYW5lbCA9IGZvY3VzUGFuZWw7XG5cbiAgICAgICAgaWYoZm9jdXNQYW5lbCAhPSBudWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICB0aGlzLnRhcmdldFBvc2l0aW9uID0gdGFyZ2V0UG9zaXRpb247XG4gICAgICAgICAgICB0aGlzLmZvY3VzUGFuZWwuR2V0RWxlbWVudCgpLnNldEF0dHJpYnV0ZShcImFjdGl2ZVwiLCB0aGlzLnRhcmdldFBvc2l0aW9uKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMudGFyZ2V0UG9zaXRpb24gPSBudWxsO1xuICAgICAgICB9XG5cbiAgICB9LFxuICAgIF9maW5kRm9jdXNQYW5lbDogZnVuY3Rpb24gKGN1clBhbmVsKVxuICAgIHtcbiAgICAgICAgdmFyIGJSZWN0ID0gY3VyUGFuZWwuR2V0RWxlbWVudCgpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuXG4gICAgICAgIGlmKE1vdXNlLmNsaWVudFggPj0gYlJlY3QueCAmJiBNb3VzZS5jbGllbnRYIDw9IGJSZWN0LnggKyBiUmVjdC53aWR0aCAmJlxuICAgICAgICAgICAgTW91c2UuY2xpZW50WSA+PSBiUmVjdC55ICYmIE1vdXNlLmNsaWVudFkgPD0gYlJlY3QueSArIGJSZWN0LmhlaWdodClcbiAgICAgICAge1xuICAgICAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGN1clBhbmVsLmNoaWxkcy5sZW5ndGg7IGkrKylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB2YXIgcGFuZWwgPSB0aGlzLl9maW5kRm9jdXNQYW5lbChjdXJQYW5lbC5jaGlsZHNbaV0pO1xuICAgICAgICAgICAgICAgIGlmKHBhbmVsICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBwYW5lbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBjdXJQYW5lbDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH0sXG4gICAgX2NhbGNUYXJnZXRQb3NpdGlvbjogZnVuY3Rpb24gKGZvY3VzUGFuZWwsIGJSZWN0KSB7XG4gICAgICAgIHZhciBtYXhCb3JkZXJXaWR0aCAgPSBNYXRoLm1pbigyMDAsIGJSZWN0LndpZHRoICogMC4yNSk7XG4gICAgICAgIHZhciBtYXhCb3JkZXJIZWlnaHQgPSBNYXRoLm1pbigyMDAsIGJSZWN0LmhlaWdodCAqIDAuMjUpO1xuXG5cbiAgICAgICAgaWYoZm9jdXNQYW5lbCA9PT0gdGhpcy5tYWluUGFuZWwgJiYgZm9jdXNQYW5lbC5mb3JtID09IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gXCJjZW50ZXJcIjtcblxuICAgICAgICBpZiggTWF0aC5hYnMoTW91c2UuY2xpZW50WCAtIChiUmVjdC54ICsgYlJlY3Qud2lkdGggLyAyKSkgPD0gIE1hdGgubWF4KGJSZWN0LndpZHRoICogMC4yNSwgKGJSZWN0LndpZHRoICogMC41IC0gbWF4Qm9yZGVyV2lkdGgpKSAmJlxuICAgICAgICAgICAgTWF0aC5hYnMoTW91c2UuY2xpZW50WSAtIChiUmVjdC55ICsgYlJlY3QuaGVpZ2h0IC8gMikpIDw9ICBNYXRoLm1heChiUmVjdC5oZWlnaHQgKiAwLjI1LCAoYlJlY3QuaGVpZ2h0ICogMC41IC0gbWF4Qm9yZGVySGVpZ2h0KSkpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiBcImNlbnRlclwiO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGxlZnRSYW5nZSA9IE1vdXNlLmNsaWVudFggLSBiUmVjdC54O1xuICAgICAgICB2YXIgcmlnaHRSYW5nZSA9IGJSZWN0LnggKyBiUmVjdC53aWR0aCAtIE1vdXNlLmNsaWVudFg7XG5cbiAgICAgICAgdmFyIHRvcFJhbmdlID0gTW91c2UuY2xpZW50WSAtIGJSZWN0Lnk7XG4gICAgICAgIHZhciBib3R0b21SYW5nZSA9IGJSZWN0LnkgKyBiUmVjdC5oZWlnaHQgLSBNb3VzZS5jbGllbnRZO1xuXG4gICAgICAgIGlmKGxlZnRSYW5nZSA8PSBtYXhCb3JkZXJXaWR0aClcbiAgICAgICAge1xuICAgICAgICAgICAgaWYodG9wUmFuZ2UgPD0gbWF4Qm9yZGVySGVpZ2h0KVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmKGxlZnRSYW5nZSA8IHRvcFJhbmdlKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwibGVmdF90b3BcIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwidG9wX2xlZnRcIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKGJvdHRvbVJhbmdlIDw9IG1heEJvcmRlckhlaWdodClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihsZWZ0UmFuZ2UgPCBib3R0b21SYW5nZSlcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcImxlZnRfYm90dG9tXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcImJvdHRvbV9sZWZ0XCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHJldHVybiBcImxlZnRcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmKHJpZ2h0UmFuZ2UgPD0gbWF4Qm9yZGVyV2lkdGgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmKHRvcFJhbmdlIDw9IG1heEJvcmRlckhlaWdodClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihyaWdodFJhbmdlIDwgdG9wUmFuZ2UpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJyaWdodF90b3BcIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwidG9wX3JpZ2h0XCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihib3R0b21SYW5nZSA8PSBtYXhCb3JkZXJIZWlnaHQpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWYocmlnaHRSYW5nZSA8IGJvdHRvbVJhbmdlKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwicmlnaHRfYm90dG9tXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcImJvdHRvbV9yaWdodFwiO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJyaWdodFwiO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYodG9wUmFuZ2UgPD0gbWF4Qm9yZGVySGVpZ2h0KVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gXCJ0b3BcIjtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmKGJvdHRvbVJhbmdlIDw9IG1heEJvcmRlckhlaWdodClcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIFwiYm90dG9tXCI7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG59O1xuXG5cbmZ1bmN0aW9uIF9kZWJvdW5jZShmdW5jKXtcbiAgICB2YXIgdGltZXI7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKGV2ZW50KXtcbiAgICAgICAgaWYodGltZXIpIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICAgIHRpbWVyID0gc2V0VGltZW91dChmdW5jLDUwMCxldmVudCk7XG4gICAgfTtcbn07XG4iXX0=
