function ChangeTheme(name)
{
    var styles = document.head.getElementsByTagName("link");
    var curLink;

    for(var i = 0; i < styles.length; i++)
    {
        var href = styles[i].getAttribute("href");
        if(href.indexOf("./Themes/") == 0 && href.indexOf("./Themes/default.css") == -1)
        {
            curLink = styles[i];
            break;
        }
    }

    var link = document.createElement("link");
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.onload = MV_OnThemeChanged;
    link.href = "./Themes/" + name + ".css";

    if(curLink === undefined)
    {
        document.head.appendChild(link);
        return;
    }

    document.head.insertBefore(link, curLink);
    document.head.removeChild(curLink);
}

function MV_OnLoad()
{
    if (window.frameElement === null)
        return;
    top.MV_OnLoad(window.frameElement.parentElement.id);
}

function MV_ChangeTheme(name)
{
    ChangeTheme(name);
}


function MV_OnThemeChanged()
{
    if (window.frameElement === null)
        return;

    top.MV_OnThemeChanged(window.frameElement.parentElement.id, name);
}




function MV_OnFocus()
{
    if (window.frameElement === null)
        return;
    top.MV_OnFocus(window.frameElement.parentElement.id);
}

function MV_OnBlur()
{
    if (window.frameElement === null)
        return;
    top.MV_OnBlur(window.frameElement.parentElement.id);
}

function MV_OnClick(e)
{
    if (window.frameElement === null)
        return;
    top.MV_OnClick(window.frameElement.parentElement.id, e);
}


function MV_OnContextMenu(e)
{
    if (window.frameElement === null)
        return;
    top.MV_OnContextMenu(window.frameElement.parentElement.id, e);
}




window.onfocus  = function()
{
    MV_OnFocus();
    console.log("onfocus");
};

window.onblur  = function()
{
    MV_OnBlur();
    console.log("onblur");
};

window.onclick  = function(e)
{
    var pb = window.frameElement.parentElement.getBoundingClientRect();

    var pointer = {
        clientX : e.clientX,
        clientY : e.clientY,
        screenX : e.clientX + pb.x,
        screenY : e.clientY + pb.y
    };

    pointer.screenX = pointer.clientX + pb.x;
    pointer.screenY = pointer.clientY + pb.y;

    MV_OnClick(pointer);
};

window.oncontextmenu  = function(e)
{
    var pb = window.frameElement.parentElement.getBoundingClientRect();

    var pointer = {
        clientX : e.clientX,
        clientY : e.clientY,
        screenX : e.clientX + pb.x,
        screenY : e.clientY + pb.y
    };

    MV_OnContextMenu(pointer);
};
