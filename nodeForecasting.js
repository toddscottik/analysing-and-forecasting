var ARIMA = require("arima");
let arima = ARIMA;
var fs = require('fs');
var brain = require('./node_modules/brain.js/dist/brain.js')
const path = require('path');
const Papa = require('papaparse');
const timeseries = require('timeseries-analysis')

let seasons = {
    'no': undefined,
    'hour': 3600000,
    'day': 86400000,
    'week': 604800000,
    'month': 2678400000,
    'year': 31536000000
}

let arrayOfMethods = [
    {method: 'Авторегрессия', str: 'ar'},
    {method: 'Скользящее среднее', str: 'ma'},
    {method: 'ARIMA', str: 'arima'},
];
let analysedData = {};
let sampleData = [];
let fd;

let jsondata = require('./data/temp_ureng.json')
let csvdata;


csvdata = fs.readFileSync('./data/temp_ureng.csv')
Papa.parse(csvdata.toString(), {
    delimiter: ';',
    dynamicTyping: true,
    complete: function (results) {
        console.log("Parsing complete:", results);
        let temp = getCSV(results);
        temp.forEach(a => {
            readyData(a);
        })
        for (const sampleDataKey of sampleData) {
            calculateStatistics(sampleDataKey.id);
        }
    }
})

function getCSV(res) {
    let temp = [];
    let t;
    for (let i = 1; i < res.data[0].length; i++) {
        if (res.data[0][i] !== null) {
            t = res.data.slice(1).map((a) => {
                if (a[i] !== null) {
                    let str = a[0].replace(/\./g,'-').split(' ')[0].split('-').reverse().join('-') + a[0].slice(10);
                    return [new Date(str).getTime(), a[i]]
                }
            });
            t.sort((a,b)=>{return a[0]-b[0]});
            t = t.filter((a) => {
                if (typeof a !== 'undefined')
                    return a[1] !== null
            })
            let moment = 0,momentValue=0,sum=0, k=0;
            let tempArray = [];
            for (let j = 0; j < t.length; j++) {
                if(j===0) {moment = t[j][0];momentValue= t[j][1]; continue}
                if ( new Date(t[j][0]) - new Date(moment)<seasons['week']){
                    sum+=t[j][1];
                    k++;
                }
                else{
                    tempArray.push([moment,Number(momentValue.toFixed(4))]);
                    sum=0;k=0;
                    moment = t[j][0];
                    momentValue = t[j][1];
                }
            }
            t = tempArray;
            temp.push({
                Name: res.data[0][i],
                value: t.filter((a) => {
                    if (typeof a !== 'undefined')
                        return a[1] !== null
                })
            })
        }
    }
    console.log(temp);
    return temp;
}

function readyData(values) {
    let d = {};
    d.id = uuidv4();
    d.caption = values.Name;
    d.points = [];
    for (let i = 0; i < values.value.length; i++) {
        d.points.push([values.value[i][0], values.value[i][1]])
    }
    sampleData.push(d);
}
function readyTSData(data) {
    let tsData = [];
    data.forEach(a => {
        tsData.push([new Date(a[0]), a[1]])
    })
    return new timeseries.main(tsData);
}

function calculateStatistics(id) {
    console.log(id);
    let series = sampleData.find(a => {
        return a.id === id
    });
    let data = readyTSData(series.points);
    analysedData[id] = {};
    analysedData[id].originalData = series.points;
    analysedData[id].data = data;
    analysedData[id].min = data.min();
    analysedData[id].max = data.max();
    analysedData[id].mean = data.mean();
    analysedData[id].std = data.stdev();
}



function drawMovAvereage(id, str,season) {
    console.time('Calculate Forecasting')
    let data = analysedData[id];
    let ts = data.data;
    let chSeries = sampleData.find(a => {
        return a.id === id
    });
    let series = [];
    let cycles;
    let step = ts.data[1][0] - ts.data[0][0];
    let numOfPoints = 0;
    let start = ts.data[numOfPoints][0];
    numOfPoints++;
    while (ts.data[numOfPoints][0] - start < seasons[season]) {
        numOfPoints++;
    }
    if (numOfPoints!==0){
        cycles = Math.ceil(ts.data.length / numOfPoints / 3);
    }
    else cycles = 300;
    ts.reset();
    switch (str) {
        case 'ma': {
            console.log("MA_Start " + season);
            console.time("MA_End");
            ts.smoother({period: Math.ceil((numOfPoints+1) / 3)});
            let temp = {
                Name: "",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }
            let arimaModel = new arima({
                auto: true,
                p: 0,
                 d:0,
                // q:0,
                // P:1,
                // D:1,
                // Q:0,
                s: season === 'no' ? undefined : numOfPoints
            });
            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(236,1330);
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict(1460);
            let pr = [];
            pr.push([+temp.value[235+trainSet.length - 1][0],temp.value[235+trainSet.length - 1][1]]);
            // for (let prElement of predicted[0]) {
            //     pr.push([+pr[pr.length - 1][0] + step, (prElement>100 || prElement<-100)?0:prElement])
            // }
            for (let i = 0; i < predicted[0].length; i++) {
                pr.push([+pr[pr.length - 1][0] + step,
                    (predicted[0][i]>100 || predicted[0][i]<-100)?predicted[0][i-1]+Math.random():predicted[0][i]])
            }
            let minErrors = [],maxErrors = [],errors = [];
            pr.forEach((a,i)=>{
                minErrors.push([a[0],a[1]-predicted[1][i]]);
                maxErrors.push([a[0],+a[1]+predicted[1][i]]);
                errors.push([a[0],a[1]-predicted[1][i],+a[1]+predicted[1][i]]);
            });
            console.log(minErrors);
            fs.writeFileSync('./data/temp_ureng'+'min'+str+season+'.json', JSON.stringify({Name:'temp_min',value:minErrors}));
            fs.writeFileSync('./data/temp_ureng'+'max'+str+season+'.json', JSON.stringify({Name:'temp_max',value:maxErrors}));
            fs.writeFileSync('./data/temp_ureng'+'errors'+str+season+'.json', JSON.stringify({Name:'temp_errors',value:errors}));

            /*series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                dashStyle: 'dot',
                yAxis: 0
            });*/
            series.push(
                {
                    id: 'MA_' + id,
                    name: 'MA ' + chSeries.caption,
                    data: pr,
                });
            console.timeEnd("MA_End");
            break
        }
        case 'ar': {
            console.log("AR_Start " + season);
            console.time("AR_End");

            ts.smoother({period: Math.floor((numOfPoints+1) / 3)});
            let temp = {
                Name: "",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }
            let arimaModel = new arima({
                auto: true,
                // p:1,
                d:0,
                q: 0,
                // P:1,
                // D:1,
                // Q:0,
                s: numOfPoints
            });
            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(230,1324);
            console.log(trainSet)
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict(1460);
            let pr = [];
            pr.push([+temp.value[230+trainSet.length - 1][0],temp.value[230+trainSet.length - 1][1]]);
            for (let prElement of predicted[0]) {
                pr.push([+pr[pr.length - 1][0] + step, prElement])
            }
            let minErrors = [],maxErrors = [],errors = [];
            pr.forEach((a,i)=>{
                minErrors.push([a[0],a[1]-predicted[1][i]]);
                maxErrors.push([a[0],+a[1]+predicted[1][i]]);
                errors.push([a[0],a[1]-predicted[1][i],+a[1]+predicted[1][i]]);
            });
            console.log(minErrors);
            fs.writeFileSync('./data/temp_ureng'+'min'+str+season+'.json', JSON.stringify({Name:'temp_min',value:minErrors}));
            fs.writeFileSync('./data/temp_ureng'+'max'+str+season+'.json', JSON.stringify({Name:'temp_max',value:maxErrors}));
            fs.writeFileSync('./data/temp_ureng'+'errors'+str+season+'.json', JSON.stringify({Name:'temp_errors',value:errors}));

            /*series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                dashStyle: 'dot',
                yAxis: 0
            });*/
            series.push(
                {
                    id: 'AR_' + id,
                    name: 'AR ' + chSeries.caption,
                    data: pr,
                });
            console.timeEnd("AR_End");
            break
        }
        case 'arima': {
            console.log("ARIMA " + season);

            ts.ma({period: Math.floor((numOfPoints+1) / 4)});

            if (season === 'no') {
                ts.smoother({period: Math.ceil(ts.original.length * 0.05)});
            }
            let pointsTRData = ts.data.slice(0, ts.original.length);
            let pointsTSData = ts.data.slice(ts.original.length);
            let temp = {
                Name: "T",
                value: []
            }
            for (let i = 0; i < ts.data.length; i += 1) {
                if (ts.data[i][1] === null) temp.value.push([ts.data[i][0], ts.data[i - 1][1]])
                else temp.value.push([ts.data[i][0], ts.data[i][1]])
            }

            let arimaModel = new arima({
                auto: true,
                method:0,
                optimizer:4,
                s: season === 'no' ? undefined : 52
            });

             fs.writeFileSync('./data/temp_ureng_with_MA_t.json', JSON.stringify({Name:temp.Name,value:temp.value.map(a=>{return [a[0].getTime(),a[1]]})}));

            let trainSet = temp.value.map(a => {
                return a[1]
            }).slice(0,208);
            let ar = arimaModel.train(trainSet);
            let predicted = arimaModel.predict(temp.value.length-208);
            console.log(predicted[0])
            console.log(predicted[1])
            let pr = [];
            pr.push([+temp.value[0+trainSet.length - 1][0],temp.value[0+trainSet.length - 1][1]]);
            let oldElement = 0;
            for (let prElement of predicted[0]) {
                if (prElement>1000) prElement = oldElement;
                pr.push([+pr[pr.length - 1][0] + step, prElement])
                oldElement = prElement;
            }
            let minErrors = [],maxErrors = [],errors = [];
            pr.forEach((a,i)=>{
                minErrors.push([a[0],a[1]-predicted[1][i]]);
                maxErrors.push([a[0],+a[1]+predicted[1][i]]);
                errors.push([a[0],a[1]-predicted[1][i],+a[1]+predicted[1][i]]);
            });
            console.log(errors);
            console.log(predicted[1])
            // fs.writeFileSync('./data/temp_ureng'+'min'+str+season+'.json', JSON.stringify({Name:'temp_min',value:minErrors}));
            // fs.writeFileSync('./data/temp_ureng'+'max'+str+season+'.json', JSON.stringify({Name:'temp_max',value:maxErrors}));
            fs.writeFileSync('./data/temp_ureng_with_MA_'+'errors'+str+season+'.json', JSON.stringify({Name:'temp_errors',value:errors}));

            predicted.forEach(a => pointsTRData.push(a));
            /*series.push({
                id: 'SMOOTHED_' + id,
                type: 'line',
                data: readyDataForChart(ts.output()),
                dashStyle: 'dot',
                yAxis: 0
            });*/
            series.push({
                id: 'ARIMA_' + id,
                name: 'ARIMA ' + chSeries.caption,
                data: pr,
            })
            break
        }

    }
     fs.writeFileSync('./data/temp_ureng_365-1825_with_MA'+series[0].name+season+'.json', JSON.stringify({Name:series[0].name+season,value:series[0].data}));
    console.timeEnd('Calculate Forecasting')
}

function main(){
    readyData(jsondata);
    for (const sampleDataKey of sampleData) {
        calculateStatistics(sampleDataKey.id);
    }
    // for (var sampleDatum of chartingData) {
    //     for (const arrayOfMethod of arrayOfMethods) {
            // for (const season in seasons) {
                drawMovAvereage(sampleData[0].id,'arima','year');
            // }
        // }
    // }
    // let data = analysedData[chartingData[1].id].data.smoother({period:10}).output();
    // console.log(data)
    //     calcNeuralNetwork(data);
}
main()


// function convertDataToNN(data) {
//     let d = [];
//     /*for (let datum of data){
//         d.push({input:[datum[0].getTime()],output:[datum[1]]})
//     }*/
//     for (let datum of data){
//         d.push(datum[1])
//     }
//     console.log(d)
//     return d;
// }
// function calcNeuralNetwork(data) {
//     const config = {
//         binaryThresh: 0.5,
//         hiddenLayers: [100], // array of ints for the sizes of the hidden layers in the network
//         activation: 'leaky-relu', // supported activation types: ['sigmoid', 'relu', 'leaky-relu', 'tanh'],
//         leakyReluAlpha: 0.01, // supported for activation type 'leaky-relu'
//     };
//
// // create a simple feed forward neural network with backpropagation
//     var net = new brain.recurrent.LSTMTimeStep();
//     net.train([convertDataToNN(data)],{log:true,timeout: 60000 });
//     let output = [];
//     let o;
//     o = net.run([convertDataToNN(data)]);
//     console.log(o);
//     // for (let i = 0; i < 365; i++) {
//     //     o = net.run([convertDataToNN(data)]);
//     //     console.log(o);
//     //     output.push(o);
//     // }
//     // fs.writeFileSync('./data/temp+ob/Neural.json', JSON.stringify({Name:'Neural',value:output}));
// }

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}